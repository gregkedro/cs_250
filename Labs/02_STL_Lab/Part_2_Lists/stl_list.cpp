// Lab - Standard Template Library - Part 2 - Lists
// GREGORY KEDROVSKY

#include <iostream>
#include <list>
#include <string>
using namespace std;

void DisplayList( list<string>& statesList );

int main()
{
    list<string> states;
    int choice(1);
    string input;

    while ( choice != 5 )
    {
        cout << "\n------------------------------\n"
             << "State list size: " << states.size() << endl << endl
             << "1. Add new state to front    2. Add new state to back\n"
             << "3. Pop front state           4. Pop back state\n"
             << "5. Continue\n\n"
             << "Your selection (1-5): ";

        cin >> choice;
        cout << endl;

        switch (choice)
        {
            // 1. push_front: add new state to front
            case 1:
                cout << "ADD STATE TO FRONT\n"
                     << "Enter new State name: ";
                cin.ignore(1000, '\n');
                getline(cin,input);
                states.push_front(input);
                cout << endl;
                break;

            // 2. push_back: add new state to back
            case 2: 
                cout << "ADD STATE TO BACK\n"
                     << "Enter new State name: ";
                cin.ignore(1000, '\n');
                getline(cin,input);
                states.push_back(input);
                cout << endl;
                break;

            // 3. pop_front: pop the front state
            case 3: 
                cout << "REMOVE STATE FROM FRONT\n"
                     << states.front() << " removed\n\n";
                states.pop_front();
                break;

            // 4. pop_back: pop the back state
            case 4: 
                cout << "REMOVE STATE FROM BACK\n"
                     << states.back() << " removed\n\n";
                states.pop_back();
                break;

            // 5. Continue: out of while loop and display
            case 5: 
                break;
            default:
                cout << "\nInvalid Entry\n";
        }
    }

    // 1. Display the original list
    cout << "ORIGINAL LIST:\n";
    DisplayList(states);

    // 2. Display the original list reversed
    cout << "REVERSE LIST:\n";
    states.reverse();
    DisplayList(states);

    // 3. Display the list sorted
    cout << "SORTED LIST:\n";
    states.sort();
    DisplayList(states);

    // 4. Display the list reverse sorted
    cout << "REVERSE-SORTED LIST:\n";
    states.reverse();
    DisplayList(states);


    cout << "Goodbye\n\n";

    return 0;
}

void DisplayList( list<string>& statesList )
{
    for ( list<string>::iterator it  = statesList.begin();
                                 it != statesList.end();
                                 it++ )
    {
        cout << *it << "\t";
    }
    cout << endl << endl;
}
