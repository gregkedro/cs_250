// Lab - Standard Template Library - Part 1 - Vectors
// Gregory Kedrovsky

#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main()
{
    int selection(3);
    string input;
    vector<string> courses;

    while ( selection != 4 )
    {
        cout << "------------------------------------\n\n"
             << "Course list size: " << courses.size() << endl << endl
             << "1. Add a new course            2. Remove last course\n"
             << "3. Display the course list     4. Quit\n\n"
             << "Your selection (1-4): ";

        cin >> selection;
		
        switch(selection)
        {
            case 1:
                cout << "\nNEW COURSE\n"
                     << "Enter new course name: ";
                cin.ignore(1000, '\n');
                getline(cin,input);
                courses.push_back(input);
                cout << endl;
                break;
            case 2:
                if ( courses.empty() )
                {
                    cout << "\nNO COURSES TO REMOVE\n";
                }
                else
                {
                    cout << "\nREMOVE COURSE\n "
                         << courses.back() << " removed";
                    courses.pop_back();
                }
                cout << endl;
                break;
            case 3:
                cout << "\nVIEW COURSE LIST:\n";
                for (int i = 0; i < (int)courses.size(); i++)
                {
                    cout << " courses[" << i << "] = " << courses[i] << endl;
                }
                cout << endl;
                break;
            case 4:
                cout << "\nGoodbye\n\n";
                break;
            default:
                cout << "\nInvalid Entry\n";
        }

    }
    return 0;
}
