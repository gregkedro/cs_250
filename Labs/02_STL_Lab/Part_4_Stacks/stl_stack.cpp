// Lab - Standard Template Library - Part 4 - Stacks
// GREGORY KEDROVSKY

#include <iostream>
#include <string>
#include <stack>
using namespace std;

int main()
{
    stack<string> word;
    string input;

    cout << "\nEnter the next letter of the word...\n"
         << " or enter UNDO to undo your last letter\n"
         << " or enter DONE if you are finished\n\n";

    // Build the Word
    do
    {
        cout << "Letter: ";
        cin >> input;
        if ( input == "UNDO" )
        {
            cout << "    Removed: " << word.top() << endl;
            word.pop();
        }
        else if ( input != "DONE" )
        {
            word.push(input);
        }
     } while ( input != "DONE" );

    // Display the Word
    cout << "\nYour word has " << word.size() << " characters: ";
    while ( !word.empty() )
    {
        cout << word.top();
        word.pop();
    }

    cout << endl << endl;

    return 0;
}
