// Lab - Standard Template Library - Part 5 - Maps
// GREGORY KEDROVSKY

#include <iostream>
#include <string>
#include <map>
using namespace std;

int main()
{
    map<char, string> colors;
    
    // Assign colors to the map
    colors['r'] = "FF0000";
    colors['g'] = "00FF00";
    colors['b'] = "0000FF";
    colors['c'] = "00FFFF";
    colors['m'] = "FF00FF";
    colors['y'] = "FFFF00";


    char input;
    do
    {
        cout << "Enter a color letter or 'q' to stop: ";
        cin >> input;
        if ( (int)colors.count(input) > 0 )
        {
            cout << "Hex: " << colors[input] << endl << endl;
        }
        else if ( input != 'q' )
        {
            // Could leave it blank, but this seems a better solution
            cout << "Not a valid key value\n\n";
        }
    } while ( input != 'q' );

    cout << "\nGoodbye\n\n";

    return 0;
}
