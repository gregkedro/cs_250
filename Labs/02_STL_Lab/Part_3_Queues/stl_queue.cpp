// Lab - Standard Template Library - Part 3 - Queues
// GREGORY KEDROVSKY

#include <iostream>
#include <string>
#include <queue>
#include <iomanip>
using namespace std;

int main()
{
    queue<float> account;
    int choice(1);
    float input(0.0);
    
    // Input transaction amounts
    while ( choice != 2 )
    {
        cout << "\n-------------------------------\n"
             << "Transactions queued: " << account.size() << endl << endl
             << "1. Enqueue transaction    2. Continue\n\n"
             << "Enter your selection (1 or 2): ";

        cin >> choice;

        if ( choice == 1 )
        {
            cout << "\nEnter amount (positive or negative) for next transaction: ";
            cin >> input;
            account.push(input);
        }
        else if ( choice != 2 )
        {
            cout << "\nInvalid Choice\n\n";
        }
    }

    // Format the output
    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);
    cout << endl;

    // Display the output
    float balance(0.0);
    while ( !account.empty() )
    {
        cout << " Pushed to account: $" << setw(10) << right << account.front() << endl;
        balance += account.front();
        account.pop();
    }

    cout << "                      ----------\n"
         << " Final Balance:     $" << setw(10) << right << balance << endl
         << "                      ==========\n"
         << "\nGoodbye\n\n";

    return 0;
}
