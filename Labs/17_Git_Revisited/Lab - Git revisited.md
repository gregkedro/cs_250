# Git, revisited

## Making your repository public

1. Log into BitBucket
2. Select your repository for this class.
3. Click on **Settings**
4. Uncheck the box that says **Access level** - This is a private repository.
5. Click **Save repository details**
6. Click the name of your repository to go back to the main pages
7. Copy the URL of this page to paste into the assignment submission

You will receive full credit for this lab if you have your labs and projects
uploaded to your class repository by the due date.

## Git basics

### Pull your repository to the computer:

On the BitBucket repository webpage, there is a **Clone** button. Clicking that will give you the clone command.
Make sure you have HTTPS selected.

The command should look like this:

```
git clone https://YOU@bitbucket.org/YOU/REPONAME.git
```

You can either open **Git Bash** or the Command Prompt on your machine and paste in this command.

Once it has executed, you will need to go *into* your project folder to do further commands:

```
cd REPONAME
```

### Add and commit files in a snapshot

Add ALL files with:

```
git add .
```

Or, add only certain types of files like this:

```
git add *.cpp *.hpp
```

This will recurse through all the subfolders in the directory that you're currently in.

Once you've added the file changes you want to preserve, you need to make a commit:

```
git commit -m "This is a message about what I updated."
```

### Get latest changes from server

If you're working across multiple machines, you should probably use git pull first:

```
git pull
```

### Push your changes to the server

After you've done an add, commit, and pull, you should push your changes to the server:

```
git push
```
