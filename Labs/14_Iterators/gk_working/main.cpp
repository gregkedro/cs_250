#include <iostream>
#include <fstream>
#include <list>
#include <string>
#include <iomanip>
using namespace std;

#include "Menu.hpp"

list<string> LoadBook( const string& filename );
void ReadBook( list<string> bookText );

int main()
{
    vector<string> books = { "aesop.txt", "fairytales.txt" };

    bool done = false;
    while ( !done )
    {
        Menu::Header( "LIBRARY" );

        cout << "Which book do you want to read?" << endl;
        int choice = Menu::ShowIntMenuWithPrompt( books );

        list<string> bookText = LoadBook( books[choice-1] );
        ReadBook( bookText );
    }

    return 0;
}


list<string> LoadBook( const string& filename )
{
    list<string> bookText;

    cout << "Loading " << filename << "..." << endl;

    ifstream input( filename );

    if ( !input.good() )
    {
        cout << "Error opening file" << endl;
    }

    string line;
    while ( getline( input, line ) )
    {
        bookText.push_back( line );
    }

    cout << endl << bookText.size() << " lines loaded" << endl << endl;

    input.close();

    return bookText;
}


void ReadBook( list<string> bookText )
{
    cout << "========================================================================" << endl;

    int counter(0),
        lines(0),
        pageLength(25);

    list<string>::iterator it;
    for ( it = bookText.begin(); it != bookText.end(); )
    {
        counter++;
        lines++;
        cout << *it << endl;
        if ( counter ==  pageLength )
        {
            cout << "------------------------------------------------------------------------" << endl
                 << " Line " << lines << " out of " << bookText.size() << endl << endl; 
           
            int choice = Menu::ShowIntMenuWithPrompt( {"BACKWARD","FORWARD"}, false );

            if ( choice == 1 ) // backward
            {
                if ( lines <= pageLength )
                {
                    for ( int i = 0; i < pageLength; i++ )
                    {
                        lines--;
                        it--;
                    }
                    
                }
                else
                    for ( int i = 0; i < pageLength*2; i++ )
                    {
                        lines--;
                        it--;
                    }
            }
            
            counter = 0;
            cout << "------------------------------------------------------------------------" << endl;
        }
        it++;
    }
}
