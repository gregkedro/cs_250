#include <iostream>
#include <string>
#include "Stack.hpp"
#include "LinkedList.hpp"
using namespace std;

int main()
{
    
    LinkedStack<string> testMe;

    // TEST #1: Size()
    cout << "-----------------------------------------------------------------------"
         << endl
         << "TEST #1: Size()" << endl;

    testMe.Display();

    cout << endl
         << " Should: 0" << endl
         << " Actual: " << testMe.Size() << endl << endl; 

    // TEST #2: Push()
    cout << "-----------------------------------------------------------------------"
         << endl
         << "TEST 2: Push() five strings" << endl << endl;

    cout << "testMe.Push(\"one\")" << endl;
    testMe.Push("one");
    testMe.Display();
    cout << endl;

    cout << "testMe.Push(\"two\")" << endl;
    testMe.Push("two");
    testMe.Display();
    cout << endl;

    cout << "testMe.Push(\"three\")" << endl;
    testMe.Push("three");
    testMe.Display();
    cout << endl;

    cout << "testMe.Push(\"four\")" << endl;
    testMe.Push("four");
    testMe.Display();
    cout << endl;

    cout << "testMe.Push(\"five\")" << endl;
    testMe.Push("five");
    testMe.Display();
    cout << endl;

    cout << " Size() Should: 5" << endl
         << " Size() Actual: " << testMe.Size() << endl << endl;


    // TEST #3: Pop()  &  TEST #4: Top()
    cout << "-----------------------------------------------------------------------"
         << endl
         << "TEST 3: Pop() three strings  &  TEST 4: Top() after Pop()" << endl << endl;

    cout << "Stack Prior to Pop()" << endl;
    testMe.Display();
    cout << " Top(): " << testMe.Top() << endl;
    cout << endl;

    cout << "testMe.Pop()" << endl;
    testMe.Pop();
    testMe.Display();
    cout << " Top(): " << testMe.Top() << endl;
    cout << endl;

    cout << "testMe.Pop()" << endl;
    testMe.Pop();
    testMe.Display();
    cout << " Top(): " << testMe.Top() << endl;
    cout << endl;

    cout << "testMe.Pop()" << endl;
    testMe.Pop();
    testMe.Display();
    cout << " Top(): " << testMe.Top() << endl;
    cout << endl;

    cout << " Size() Should: 2" << endl
         << " Size() Actual: " << testMe.Size() << endl << endl;

    // TEST #5: Clear()
    cout << "-----------------------------------------------------------------------"
         << endl
         << "TEST 5: Clear()" << endl << endl;

    cout << "Stack Prior to Clear()" << endl;
    testMe.Display();
    cout << endl;

    testMe.Clear();
    cout << "Stack After Clear()" << endl;
    testMe.Display();
    cout << endl;

    cout << " Size() Should: 0" << endl
         << " Size() Actual: " << testMe.Size() << endl << endl;


    return 0;
}
