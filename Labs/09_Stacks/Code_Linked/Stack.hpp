#ifndef _STACK_HPP
#define _STACK_HPP

#include <stdexcept>
#include "Node.hpp"
// #include "../EXCEPTIONS/CourseNotFoundException.hpp"
using namespace std;

template <typename T>
class LinkedStack
{
    public:
        LinkedStack();
        ~LinkedStack();

        void Push( const T& newData ) noexcept;
        void Pop() noexcept;

        T& Top();
        int Size();
        void Clear();
        void Display();

    private:
        Node<T>* m_ptrFirst;
        Node<T>* m_ptrLast;
        int m_itemCount;
};

template <typename T>
LinkedStack<T>::LinkedStack()
{
    m_ptrFirst  = nullptr;
    m_ptrLast   = nullptr;
    m_itemCount = 0;
}

template <typename T>
LinkedStack<T>::~LinkedStack()
{
    Clear();
}

template <typename T>
void LinkedStack<T>::Push( const T& newData ) noexcept
{
    // Create a new node
    Node<T>* ptrNewNode = new Node<T>;
    ptrNewNode->data    = newData;

    // ====== EMPTY STACK ======
    if ( m_itemCount == 0 )
    {
        m_ptrFirst = ptrNewNode;
        m_ptrLast  = ptrNewNode;
    }    

    // ====== !EMPTY STACK =====
    else 
    {
        ptrNewNode->ptrPrev = m_ptrLast; // new node points back to old last
        m_ptrLast->ptrNext = ptrNewNode; // old last points to new node
        m_ptrLast          = ptrNewNode; // LinkedStack last pointer points to new node
    }

    m_itemCount++;
    return;
}

template <typename T>
T& LinkedStack<T>::Top()
{
    if ( m_itemCount < 1 )
        throw out_of_range( "Stack is Empty. No Top." );
    else
        return m_ptrLast->data; // returns the _value_ of the top item in stack
}

template <typename T>
void LinkedStack<T>::Pop() noexcept
{
    // ===== EMPTY STACK =====
    if ( m_itemCount == 0 )
        return;

    // ===== STACK WITH ONE ITEM =====
    else if ( m_itemCount == 1 )
    {
        // 1. Delete node
        delete m_ptrLast;

        // 2. Adjust LinkedStack poitners
        m_ptrLast  = nullptr;
        m_ptrFirst = nullptr;

        // 3. Housekeeping
        m_itemCount--;
    }

    // ===== STACK WITH MORE THAN ONE ITEM =====
    else // more than one node
    {
        Node<T>* nodeToDelete = m_ptrLast;

        // 1. Adjust LinkedStack pointer
        m_ptrLast = m_ptrLast->ptrPrev;

        // 2. Adjust new last node pointer
        m_ptrLast->ptrNext = nullptr;

        // 3. Delete popped node
        delete nodeToDelete;
        nodeToDelete->ptrPrev = nullptr;
        nodeToDelete->ptrNext = nullptr;

        // 4. Housekeeping
        m_itemCount--;

        return;
    }
}

template <typename T>
int LinkedStack<T>::Size()
{
    return m_itemCount;
}

template <typename T>
void LinkedStack<T>::Clear()
{
    while ( m_ptrLast != nullptr )
        Pop();
    return;
}

template <typename T>
void LinkedStack<T>::Display()
{

    if ( m_itemCount < 1 )
    {
        cout << " LinkedStack is Empty." << endl;
    }
    else
    {
        Node<T>* current = m_ptrLast;
        int counter = m_itemCount;
        while ( current != nullptr )
        {
            cout << " " << counter << ". " << current->data << endl;
            counter--;
            current = current->ptrPrev;
        }    
    }
    return;
}

#endif
