#ifndef _STACKARRY_HPP
#define _STACKARRY_HPP

#include <stdexcept>
using namespace std;

template <typename T>
class ArrayStack
{
    public:
        ArrayStack();
        ~ArrayStack();

        void Push( const T& newData ) noexcept;
        void Pop() noexcept;

        T& Top();
        int Size();
        void Clear();
        void Display();

    private:
        T* m_array;

        int m_arraySize;
        int m_itemCount;

        void m_allocate();
        void m_resize();
};

template <typename T>
ArrayStack<T>::ArrayStack()
{
    m_array = nullptr;  // no items, no memory needed yet
    m_arraySize = 5;    // set initial array size
    m_itemCount = 0;    // set item count to zero
}

template <typename T>
ArrayStack<T>::~ArrayStack()
{
    if ( m_array != nullptr )
    {
        delete [] m_array;
        m_array = nullptr;
    }
}

template <typename T>
void ArrayStack<T>::Push( const T& newData ) noexcept
{
    // Check if array has not been allocated (new array)
    if ( m_array == nullptr )
    {
        m_allocate();
    }

    // Check if array is full
    if ( m_itemCount == m_arraySize )
    {
        m_resize();
    }

    // Add new data (push to top of stack)    
    m_array[ m_itemCount ] = newData;
    m_itemCount++;

    return;
}

template <typename T>
void ArrayStack<T>::m_resize()
{
    m_arraySize+=5; 

    T* newArray = new T[m_arraySize];

    for ( int i = 0; i < m_itemCount; i++ )
    {
        newArray[i] = m_array[i];
    }

    delete[] m_array;
    m_array = newArray;           

    return;
}

template <typename T>
T& ArrayStack<T>::Top()
{
    if ( m_array == nullptr || m_itemCount == 0 )
    {
        throw out_of_range( "Stack is Empty. No Top." );
    }
    else
    {
        return m_array[ m_itemCount - 1 ]; // returns the _value_ of the top item in stack
    }
}

template <typename T>
void ArrayStack<T>::Pop() noexcept
{
    // Empty Stack
    if ( m_array == nullptr || m_itemCount == 0 )
    {
        return;
    }

    // Stack with one or more item(s)
    else 
    {
        m_itemCount--;
        return;
    }
}

template <typename T>
int ArrayStack<T>::Size()
{
    return m_itemCount;
}

template <typename T>
void ArrayStack<T>::Clear()
{
    m_itemCount = 0;
    return;
}

template <typename T>
void ArrayStack<T>::m_allocate()
{
    m_array = new T[ m_arraySize ];
    return;
}

template <typename T>
void ArrayStack<T>::Display()
{
    if ( m_itemCount < 1 )
    {
        cout << " ArrayStack is Empty." << endl;
    }
    else
    {
        for ( int i = m_itemCount; i > 0; i-- )
        {
            cout << " " << i << ". " << m_array[i-1] << endl;
        }
    }
    return;
}

#endif
