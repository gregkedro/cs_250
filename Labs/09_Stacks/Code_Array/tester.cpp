#include <iostream>
#include <string>
#include "StackArray.hpp"
using namespace std;

int main()
{
    
    ArrayStack<string> testMe;

    // TEST #1: Size()
    cout << "-----------------------------------------------------------------------"
         << endl
         << "TEST #1: Size()" << endl;

    testMe.Display();

    cout << endl
         << " Should: 0" << endl
         << " Actual: " << testMe.Size() << endl << endl; 

    // TEST #2: Push()
    cout << "-----------------------------------------------------------------------"
         << endl
         << "TEST 2: Push() five strings" << endl << endl;

    cout << "testMe.Push(\"one\")" << endl;
    testMe.Push("one");
    testMe.Display();
    cout << endl;

    cout << "testMe.Push(\"two\")" << endl;
    testMe.Push("two");
    testMe.Display();
    cout << endl;

    cout << "testMe.Push(\"three\")" << endl;
    testMe.Push("three");
    testMe.Display();
    cout << endl;

    cout << "testMe.Push(\"four\")" << endl;
    testMe.Push("four");
    testMe.Display();
    cout << endl;

    cout << "testMe.Push(\"five\")" << endl;
    testMe.Push("five");
    testMe.Display();
    cout << endl;

    cout << " Size() Should: 5" << endl
         << " Size() Actual: " << testMe.Size() << endl << endl;


    // TEST #3: Pop()  &  TEST #4: Top()
    cout << "-----------------------------------------------------------------------"
         << endl
         << "TEST 3: Pop() three strings  &  TEST 4: Top() after Pop()" << endl << endl;

    cout << "Stack Prior to Pop()" << endl;
    testMe.Display();
    cout << " Top(): " << testMe.Top() << endl;
    cout << endl;

    cout << "testMe.Pop()" << endl;
    testMe.Pop();
    testMe.Display();
    cout << " Top(): " << testMe.Top() << endl;
    cout << endl;

    cout << "testMe.Pop()" << endl;
    testMe.Pop();
    testMe.Display();
    cout << " Top(): " << testMe.Top() << endl;
    cout << endl;

    cout << "testMe.Pop()" << endl;
    testMe.Pop();
    testMe.Display();
    cout << " Top(): " << testMe.Top() << endl;
    cout << endl;

    cout << " Size() Should: 2" << endl
         << " Size() Actual: " << testMe.Size() << endl << endl;

    // TEST #5: Clear()
    cout << "-----------------------------------------------------------------------"
         << endl
         << "TEST 5: Clear()" << endl << endl;

    cout << "Stack Prior to Clear()" << endl;
    testMe.Display();
    cout << endl;

    testMe.Clear();
    cout << "Stack After Clear()" << endl;
    testMe.Display();
    cout << endl;

    cout << " Size() Should: 0" << endl
         << " Size() Actual: " << testMe.Size() << endl << endl;

    // TEST #6: Resize
    cout << "-----------------------------------------------------------------------"
         << endl
         << "TEST 6: Resize" << endl << endl;

    cout << "Stack Prior to resizing:" << endl;
    testMe.Display();
    cout << endl;

    cout << "Push() five strings" << endl << endl;

    cout << "testMe.Push(\"one\")" << endl;
    testMe.Push("one");
    testMe.Display();
    cout << endl;

    cout << "testMe.Push(\"two\")" << endl;
    testMe.Push("two");
    testMe.Display();
    cout << endl;

    cout << "testMe.Push(\"three\")" << endl;
    testMe.Push("three");
    testMe.Display();
    cout << endl;

    cout << "testMe.Push(\"four\")" << endl;
    testMe.Push("four");
    testMe.Display();
    cout << endl;

    cout << "testMe.Push(\"five\")" << endl;
    testMe.Push("five");
    testMe.Display();
    cout << endl;

    cout << " Size() Should: 5" << endl
         << " Size() Actual: " << testMe.Size() << endl << endl;

    cout << "Prepped and ready with a full array (m_arraySize == 5)..." << endl << endl;

    cout << "Push() five more strings" << endl << endl;

    cout << "testMe.Push(\"six\")" << endl;
    testMe.Push("six");
    testMe.Display();
    cout << endl;

    cout << "testMe.Push(\"seven\")" << endl;
    testMe.Push("seven");
    testMe.Display();
    cout << endl;

    cout << "testMe.Push(\"eight\")" << endl;
    testMe.Push("eight");
    testMe.Display();
    cout << endl;

    cout << "testMe.Push(\"nine\")" << endl;
    testMe.Push("nine");
    testMe.Display();
    cout << endl;

    cout << "testMe.Push(\"ten\")" << endl;
    testMe.Push("ten");
    testMe.Display();
    cout << endl;

    cout << "After resize..." << endl
         << " Size() Should: 10" << endl
         << " Size() Actual: " << testMe.Size() << endl << endl;

    cout << "Add one more and the array should resize again to accomdate:" << endl;

    cout << "testMe.Push(\"eleven\")" << endl;
    testMe.Push("eleven");
    testMe.Display();
    cout << endl;

    cout << "After resize..." << endl
         << " Size() Should: 11" << endl
         << " Size() Actual: " << testMe.Size() << endl << endl;


    return 0;
}
