#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <fstream>
#include <map>
using namespace std;

#include "Timer.hpp"
#include "Menu.hpp"

struct DataEntry
{
    map<string, string> fields;
    // one line of csv file per struct (entry)
    // each struct is an element in a vector (data)
    // the map is key:value (header_title:value)

    void Output( ofstream& output )
    {
        for ( map<string, string>::iterator it = fields.begin();
                it != fields.end(); it++ )
        {
            output << left << setw( 30 ) << it->second;
        }
        output << endl;
    }
};

void ReadData( vector<DataEntry>& data, const string& filename );

/* ********************************************* */
/* TODO: Add prototypes for sort functions here. */
/* ********************************************* */
// void SelectionSort( vector<DataEntry>& data, const string& onKey );
void BubbleSort( vector<DataEntry>& data, const string& onKey );
void QuickSort( vector<DataEntry>& data, const string& onKey );
void QuickSort_Recursive( vector<DataEntry>& data, int low, int high, const string& onKey );
int Partition( vector<DataEntry>& data, int low, int high, const string& onKey );


int main()
{
    Menu::Header( "U.S. Chronic Disease Indicators" );

    vector<string> filenames = {
        "100_US_Chronic_Disease_Indicators.csv",
        "1000_US_Chronic_Disease_Indicators.csv",
        "10000_US_Chronic_Disease_Indicators.csv",
        "523487_US_Chronic_Disease_Indicators.csv"
    };

    /* ********************************************* */
    /* TODO: Update this menu with your sorting algs */
    /* ********************************************* */
    vector<string> sorts = {
        "Bubble Sort (basic)",
        "Quick Sort  (faster)"
    };

    vector<string> columns = {
        "YearStart", "YearEnd", "LocationAbbr", "LocationDesc", "Topic", "Question"
    };

    cout << "Which file do you want to load?" << endl;
    int fileChoice = Menu::ShowIntMenuWithPrompt( filenames );

    cout << "Which sort do you want to use?" << endl;
    int sortChoice = Menu::ShowIntMenuWithPrompt( sorts );

    cout << "Which column do you want to sort on?" << endl;
    int sortOnChoice = Menu::ShowIntMenuWithPrompt( columns );

    cout << "Your choices:" << endl
         << " File   Choice: " << fileChoice << endl
         << " Sort   Choice: " << sortChoice << endl
         << " SortOn Choice: " << sortOnChoice << endl << endl;

    Timer timer;
    vector<DataEntry> data;

    string filename = filenames[ fileChoice - 1 ];


    // Read in the data from the file
    cout << left << setw( 15 ) << "BEGIN:" 
         << "Loading data from file, \"" 
         << filename << "\"..." << endl;

    timer.Start();
    ReadData( data, filename );  // see below: build vector data from DataEntry structs

    cout << left << setw( 15 ) << "COMPLETED:" 
         << "In " << timer.GetElapsedMilliseconds() << " milliseconds" 
         << endl;

    cout << setw( 15 ) << " " <<  data.size() << " items loaded" << endl << endl;


    // Sort the data

    /* GAK: Selection Sort removed to comply with Lab instructions
    if ( sortChoice == 1 )
    {
        cout << left << setw( 15 ) << "BEGIN:" << "Sorting data with Selection Sort..." << endl;
        timer.Start();
        SelectionSort( data, columns[ sortOnChoice - 1 ] );
        cout << left << setw( 15 ) 
             << "COMPLETED:" << "In " << timer.GetElapsedMilliseconds() 
             << " milliseconds" << endl << endl;
    }
    */

    if ( sortChoice == 1 )
    {
        cout << left << setw( 15 ) << "BEGIN:" << "Sorting data with Bubble Sort..." << endl;
        timer.Start();
        /* **************************************** */
        /* TODO: Call basic sorting algorithm here */
        /* **************************************** */
        BubbleSort( data, columns[ sortOnChoice - 1 ] );
        cout << left << setw( 15 ) 
             << "COMPLETED:" << "In " << timer.GetElapsedMilliseconds() 
             << " milliseconds" << endl << endl;
    }

    else if ( sortChoice == 2 )
    {
        cout << left << setw( 15 ) << "BEGIN:" << "Sorting data with Quick Sort..." << endl;
        timer.Start();
        /* **************************************** */
        /* TODO: Call faster sorting algorithm here */
        /* **************************************** */
        QuickSort( data, columns[ sortOnChoice - 1 ] );
        cout << left << setw( 15 ) 
             << "COMPLETED:" << "In " << timer.GetElapsedMilliseconds() 
             << " milliseconds" << endl << endl;
    }
    cout << endl << "Writing list out to \"output.txt\"..." << endl;


    // Output the sorted data
    ofstream output( "output.txt" );
    for ( map<string, string>::iterator it = data[0].fields.begin();
            it != data[0].fields.end(); it++ )
    {
        output << left << setw( 30 ) << it->first;
    }

    output << endl;

    for ( unsigned int i = 0; i < data.size(); i++ )
    {
        data[i].Output( output );
    }
    output.close();

    cout << endl;

    Menu::Header( "Hit ENTER to quit." );

    cin.ignore();
    cin.get();

    return 0;
}

void ReadData( vector<DataEntry>& data, const string& filename )
{
    ifstream input( filename );

    vector<string> headerItems;

    unsigned int field = 0;

    string line;
    bool header = true;
    bool skippedGeoComma = false;
    while ( getline( input, line ) )
    {
        DataEntry entry;

        int columnBegin = 0;
        field = 0;
        skippedGeoComma = false;

        for ( unsigned int i = 0; i < line.size(); i++ )
        {
            //cout << line[i];
            if ( line[i] == ',' )
            {
                int length = i - columnBegin;
                string substring = line.substr( columnBegin, length );

                if ( header )
                {
                    headerItems.push_back( substring );
                }
                else
                {
                    string fieldKey = "";
                    if (field >= headerItems.size())
                    {
                        fieldKey = "Unknown";
                    }
                    else
                    {
                        fieldKey = headerItems[field];
                    }
                    entry.fields[ fieldKey ] = substring;
                }

                columnBegin = i+1;

                if ( header == false && skippedGeoComma == false && headerItems[ field ] == "GeoLocation" )
                {
                    skippedGeoComma = true;
                    // Ignore this comma.
                    continue;
                }
                else
                {
                    field++;
                }
            }
        }

        if ( header )
        {
            header = false;
        }
        else
        {
            data.push_back( entry );  // push_back the DataEntry struct we just built
        }                             // into the data vector
    }

    input.close();
}

/* ******************************************** */
/* TODO: Implement sorting algorithsm down here */
/* ******************************************** */

/* GAK: Removed to comply with Lab instructions
void SelectionSort( vector<DataEntry>& data, const string& onKey )
{
    int n = static_cast<int>( data.size() );
    int iMin;

    for ( int j = 0; j < n-1; j++ )
    {
        // Set the current minimum as the next value in the unsorted portion of the array
        iMin = j; 

        // Search unsorted portion of array for a minimum value
        for ( int i = j+1; i < n; i++ ) 
        {
            if ( data[i].fields[ onKey ] < data[iMin].fields[ onKey ] )
            {
                iMin = i;
            }
        }

        // If we have a new minimum value, swap it with j in the array
        if ( iMin != j )
        {
            DataEntry temp = data[j]; // move j into temp (out of its place in array)
            data[j] = data[iMin];     // new min element replaces j (now sorted)
            data[iMin] = temp;        // place j where new min value was taken from
        }

        // If we do not have a new minimum value, the current value j stays where it is
    }
}
*/


/* GAK: BubbleSort
   Implementation from GeeksForGeeks
   https://www.geeksforgeeks.org/bubble-sort/ */
void BubbleSort( vector<DataEntry>& data, const string& onKey )
{
    int n = static_cast<int>( data.size() );

    for ( int i = 0; i < n-1; i++ )
    {
        for ( int j = 0; j < n-i-1; j++ )
        {
            if ( data[j].fields[ onKey ]  >  data[j+1].fields[ onKey ] )
            {
                DataEntry temp = data[j];
                data[j] = data[j+1];
                data[j+1] = temp;
            }
        }
    }

    return;
}

/* GAK: QuickSort
   Implementation from GeeksForGeeks
   https://www.geeksforgeeks.org/quick-sort/  */
void QuickSort( vector<DataEntry>& data, const string& onKey )
{
    int n = static_cast<int>( data.size() );
    QuickSort_Recursive( data, 0, n-1, onKey );
    return;
}

void QuickSort_Recursive( vector<DataEntry>& data, int low, int high, const string& onKey )
{
    if ( low < high )
    {
        int pivotIndex = Partition( data, low, high, onKey );
        QuickSort_Recursive( data, low, pivotIndex - 1, onKey );
        QuickSort_Recursive( data, pivotIndex + 1, high, onKey );
    }
    return;
}

int Partition( vector<DataEntry>& data, int low, int high, const string& onKey )
{
    DataEntry pivotElement = data[high];

    int i = (low - 1); // i keeps track of the left partition (less than pivot)

    for ( int j = low; j <= high - 1; j++ )  // j walks up the array, checking each
    {
        if ( data[j].fields[ onKey ] <= pivotElement.fields[ onKey ] )
        {
            // move i forward one since we added one to the left partition
            i++;

            // swap i and j: places new item in new i-index, in left partition
            DataEntry temp = data[i];
            data[i] = data[j];
            data[j] = temp;
        }
    }

    // place pivot to the right of i (left partition; <= than pivot)
    // move the element in i+1 (>= pivot) to the end where the pivot was
    DataEntry temp = data[i + 1];
    data[i + 1] = data[high]; 
    data[high] = temp;

    return ( i + 1 );  // return the pivot index that will divide the array 
                       // for further sorting through recursion
}
