#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <fstream>
#include <map>
using namespace std;

#include "Timer.hpp"
#include "Menu.hpp"

struct DataEntry
{
    map<string, string> fields;
    // one line of csv file per struct (entry)
    // each struct is an element in a vector (data)
    // the map is key:value (header_title:value)

    void Output( ofstream& output )
    {
        for ( map<string, string>::iterator it = fields.begin();
                it != fields.end(); it++ )
        {
            output << left << setw( 30 ) << it->second;
        }
        output << endl;
    }
};

void ReadData( vector<DataEntry>& data, const string& filename );

/* ********************************************* */
/* TODO: Add prototypes for sort functions here. */
/* ********************************************* */
void SelectionSort( vector<DataEntry>& data, const string& onKey );
void BubbleSort( vector<DataEntry>& data, const string& onKey );


int main()
{
    Menu::Header( "U.S. Chronic Disease Indicators" );

    vector<string> filenames = {
        "100_US_Chronic_Disease_Indicators.csv",
        "1000_US_Chronic_Disease_Indicators.csv",
        "10000_US_Chronic_Disease_Indicators.csv",
        "523487_US_Chronic_Disease_Indicators.csv"
    };

    /* ********************************************* */
    /* TODO: Update this menu with your sorting algs */
    /* ********************************************* */
    vector<string> sorts = {
        "Selection Sort",
        "Bubble Sort (basic)",
        "Quick Sort  (faster)"
    };

    vector<string> columns = {
        "YearStart", "YearEnd", "LocationAbbr", "LocationDesc", "Topic", "Question"
    };

    cout << "Which file do you want to load?" << endl;
    int fileChoice = Menu::ShowIntMenuWithPrompt( filenames );

    cout << "Which sort do you want to use?" << endl;
    int sortChoice = Menu::ShowIntMenuWithPrompt( sorts );

    cout << "Which column do you want to sort on?" << endl;
    int sortOnChoice = Menu::ShowIntMenuWithPrompt( columns );

    cout << "Your choices:" << endl
         << " File   Choice: " << fileChoice << endl
         << " Sort   Choice: " << sortChoice << endl
         << " SortOn Choice: " << sortOnChoice << endl << endl;

    Timer timer;
    vector<DataEntry> data;

    string filename = filenames[ fileChoice - 1 ];


    // Read in the data from the file
    cout << left << setw( 15 ) << "BEGIN:" 
         << "Loading data from file, \"" 
         << filename << "\"..." << endl;

    timer.Start();
    ReadData( data, filename );  // see below: build vector data from DataEntry structs

    cout << left << setw( 15 ) << "COMPLETED:" 
         << "In " << timer.GetElapsedMilliseconds() << " milliseconds" 
         << endl;

    cout << setw( 15 ) << " " <<  data.size() << " items loaded" << endl << endl;


    // Sort the data
    if ( sortChoice == 1 )
    {
        cout << left << setw( 15 ) << "BEGIN:" << "Sorting data with Selection Sort..." << endl;
        timer.Start();
        SelectionSort( data, columns[ sortOnChoice - 1 ] );
        cout << left << setw( 15 ) 
             << "COMPLETED:" << "In " << timer.GetElapsedMilliseconds() 
             << " milliseconds" << endl << endl;
    }

    else if ( sortChoice == 2 )
    {
        cout << left << setw( 15 ) << "BEGIN:" << "Sorting data with Bubble Sort..." << endl;
        timer.Start();
        /* **************************************** */
        /* TODO: Call basic sorting algorithm here */
        /* **************************************** */
        // example: BubbleSort( data, columns[ sortOnChoice - 1 ] );
        BubbleSort( data, columns[ sortOnChoice - 1 ] );
        cout << left << setw( 15 ) 
             << "COMPLETED:" << "In " << timer.GetElapsedMilliseconds() 
             << " milliseconds" << endl << endl;
    }

    else if ( sortChoice == 3 )
    {
        cout << left << setw( 15 ) << "BEGIN:" << "Sorting data with Merge Sort..." << endl;
        timer.Start();
        /* **************************************** */
        /* TODO: Call faster sorting algorithm here */
        /* **************************************** */
        // example: QuickSort( data, columns[ sortOnChoice - 1 ] );
        cout << left << setw( 15 ) 
             << "COMPLETED:" << "In " << timer.GetElapsedMilliseconds() 
             << " milliseconds" << endl << endl;
    }
    cout << endl << "Writing list out to \"output.txt\"..." << endl;


    // Output the sorted data
    ofstream output( "output.txt" );
    for ( map<string, string>::iterator it = data[0].fields.begin();
            it != data[0].fields.end(); it++ )
    {
        output << left << setw( 30 ) << it->first;
    }

    output << endl;

    for ( unsigned int i = 0; i < data.size(); i++ )
    {
        data[i].Output( output );
    }
    output.close();

    cout << endl;

    Menu::Header( "Hit ENTER to quit." );

    //cin.ignore();
    cin.get();

    return 0;
}

void ReadData( vector<DataEntry>& data, const string& filename )
{
    ifstream input( filename );

    vector<string> headerItems;

    unsigned int field = 0;

    string line;
    bool header = true;
    bool skippedGeoComma = false;
    while ( getline( input, line ) )
    {
        DataEntry entry;

        int columnBegin = 0;
        field = 0;
        skippedGeoComma = false;

        for ( unsigned int i = 0; i < line.size(); i++ )
        {
            //cout << line[i];
            if ( line[i] == ',' )
            {
                int length = i - columnBegin;
                string substring = line.substr( columnBegin, length );

                if ( header )
                {
                    headerItems.push_back( substring );
                }
                else
                {
                    string fieldKey = "";
                    if (field >= headerItems.size())
                    {
                        fieldKey = "Unknown";
                    }
                    else
                    {
                        fieldKey = headerItems[field];
                    }
                    entry.fields[ fieldKey ] = substring;
                }

                columnBegin = i+1;

                if ( header == false && skippedGeoComma == false && headerItems[ field ] == "GeoLocation" )
                {
                    skippedGeoComma = true;
                    // Ignore this comma.
                    continue;
                }
                else
                {
                    field++;
                }
            }
        }

        if ( header )
        {
            header = false;
        }
        else
        {
            data.push_back( entry );  // push_back the DataEntry struct we just built
        }                             // into the data vector
    }

    input.close();
}

/* ******************************************** */
/* TODO: Implement sorting algorithsm down here */
/* ******************************************** */

void SelectionSort( vector<DataEntry>& data, const string& onKey )
{
    int n = static_cast<int>( data.size() );

    cout << endl 
         << " SORTING ON:  " << onKey                 << endl 
         << " First Value: " << data[0].fields[onKey] << endl
         << endl;

    for ( int j = 0; j < n-1; j++ )
    {
        cout << endl << "  j = " << j << endl;  

        int iCheck = j; // move up the vector, one by one, replacing each w/ smallest
        for ( int i = j+1; i < n; i++ ) // compare each element
        {
            cout << "  i = " << i << endl;
            for ( int g = 0; g < i; g++ )
            {
                cout << "   data[" << g << "].fields[" << onKey << "] = "
                     << data[g].fields[onKey] << endl;
            }

            cout << "   iCheck = " << data[iCheck].fields[ onKey ] << " [" << iCheck << "] " << endl;

            cout << "    Compare: data[" << i << "] < " << "data[" << iCheck << "]" << endl;

            cout << "    Compare: " << data[i].fields[ onKey ] << " < " <<  data[iCheck].fields[ onKey ] << endl;

            if ( data[i].fields[ onKey ] < data[iCheck].fields[ onKey ] )
            {
                iCheck = i;
                //cout << "  (new iCheck = " << data[iCheck].fields[ onKey ] << ")" << endl;
            }

            if ( iCheck != j ) // if we find a new smallest object, swap with current smallest
            {
                //cout << "              swap: iCheck was " << data[iCheck].fields[ onKey ];
            
                DataEntry temp = data[j]; // move j (comparison element) into temp
                data[j] = data[iCheck];     // new min element goes into j (comparison element)
                data[iCheck] = temp;        // place j where new min was

                //cout << " | now iCheck is " << data[iCheck].fields[ onKey ] << endl;

            }
            cin.get();
        }
    }
    cout << endl << endl;
}

/* GAK: BubbleSort
   Implementation from GeeksForGeeks
   https://www.geeksforgeeks.org/bubble-sort/ */
void BubbleSort( vector<DataEntry>& data, const string& onKey )
{
    int n = static_cast<int>( data.size() );

    for ( int i = 0; i < n-1; i++ )
    {
        for ( int j = 0; j < n-i-1; j++ )
        {
            if ( data[j].fields[ onKey ]  >  data[j+1].fields[ onKey ] )
            {
                DataEntry temp = data[j];
                data[j] = data[j+1];
                data[j+1] = temp;
            }
        }
    }

    return;
}



// GAK: QuickSort

