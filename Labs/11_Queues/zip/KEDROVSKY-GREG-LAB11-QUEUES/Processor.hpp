#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
#include <stdexcept>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"

class Processor
{
    public:
    void FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile );
    void RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile );

    private:
    // Added by GK for ease of output
    void fcfsStats( ostream& out, vector<Job>& allJobs, int cycles );
    void rrStats( ostream& out, vector<Job>& allJobs, int cycles, int timePerProcess );
};


// ========== FCFS ====================================================

void Processor::FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile )
{
    // Open the file
    ofstream out_stream( logFile );

    // Check to make sure we have an open file
    if ( !out_stream )
    {
        throw runtime_error( "FCFS file not opened.");
    }

    // Process
    int cycles(0);

    out_stream << "Processing First Come First Served (FCFS): " 
               << jobQueue.Size() << " jobs" << endl << endl;

    out_stream << "-----------------------------------------" << endl
               << "Processing Job #"  << jobQueue.Front()->id 
               << " (time required: " << jobQueue.Front()->fcfs_timeRemaining << ")" << endl;

    while ( jobQueue.Size() > 0 )
    {
        out_stream << "  Cycle  "        << setw(7) << left << cycles
                   << "Time Remaining: " << setw(4) << left << jobQueue.Front()->fcfs_timeRemaining
                   << "  ..." << endl;

        jobQueue.Front()->Work( FCFS);
        cycles++; // each cycle is one unit of time; keep track of total time/cycles

        if ( jobQueue.Front()->fcfs_done )
        {
            out_stream << " *Cycle  "        << setw(7) << left << cycles
                       << "Time Remaining: " << setw(4) << left << jobQueue.Front()->fcfs_timeRemaining
                       << " Done" << endl << endl;
            // This will show a duplicate cycle number because two things are happening
            // during this same cycle:
            //   [1] work on the current job that zeros it out, and
            //   [2] removing current/finished job and moving the next one to the front.
            // Therefore, the next job will show a full time remaining on the same cycle
            //   the previous job shows a zero time remaining.

            jobQueue.Front()->SetFinishTime( cycles, FCFS );
            jobQueue.Pop();

            // if we have more jobs in the queue, provide header information
            if ( jobQueue.Size() > 0 )
            {
                out_stream << "-----------------------------------------" << endl
                           << "Processing Job #" << jobQueue.Front()->id 
                           << " (time required: " << jobQueue.Front()->fcfs_timeRemaining << ")" 
                           << endl;
            }
        }
    }

    // Summary Stats: out_stream
    fcfsStats( out_stream, allJobs, cycles);

    // Summary Stats: cout
    fcfsStats( cout, allJobs, cycles );

    // Close the file
    out_stream.close();
}


// ========== Round Robin =============================================

void Processor::RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile )
{
    // Open the file
    ofstream out_stream( logFile );

    // Check to make sure we have an open file
    if ( !out_stream )
    {
        throw runtime_error( "Round Robin file not opened.");
    }

    // Process
    int cycles(0);
    int timer(0);

    out_stream << "Processing Round Robin (RR): " 
               << jobQueue.Size() << " jobs" << endl << endl;

    out_stream << "-----------------------------------------------------------------" << endl
               << "Processing..." << endl;

    while ( jobQueue.Size() > 0 )
    {
        if ( timer == timePerProcess )
        {
            jobQueue.Front()->rr_timesInterrupted++;
            jobQueue.Push( jobQueue.Front() );
            jobQueue.Pop();
            timer = 0;

            out_stream << endl
                       << "-----------------------------------------------------------------" << endl
                       << "Processing..." << endl;
        }

        if ( jobQueue.Front()->rr_done )
        {
            out_stream << " *Cycle  "        << setw(7) << left << cycles
                       << "Job: "            << setw(5) << left << jobQueue.Front()->id
                       << "Remaining: " << setw(4) << left << jobQueue.Front()->rr_timeRemaining
                       << "Interrupt: " << setw(5) << left << jobQueue.Front()->rr_timesInterrupted
                       << " Done" << endl;

            jobQueue.Front()->SetFinishTime( cycles, RR );
            jobQueue.Pop();

            // If we still have jobs in the queue, show new job at the front
            // NOTE: This will show as two line of output since two things are happening:
            //   [1] work on the current job that zeros it out, and
            //   [2] removing current/finished job and moving the next one to the front.
            if ( jobQueue.Size() > 0 )
            {
                out_stream << "                Job: "            << setw(5) << left << jobQueue.Front()->id
                           << "Remaining: " << setw(4) << left << jobQueue.Front()->rr_timeRemaining
                           << "Interrupt: " << setw(5) << left << jobQueue.Front()->rr_timesInterrupted
                           << "  ..." << endl;
            }
        }
        else
        {
            out_stream << "  Cycle  "        << setw(7) << left << cycles
                       << "Job: "            << setw(5) << left << jobQueue.Front()->id
                       << "Remaining: " << setw(4) << left << jobQueue.Front()->rr_timeRemaining
                       << "Interrupt: " << setw(5) << left << jobQueue.Front()->rr_timesInterrupted
                       << "  ..." << endl;
        }

        // Safeguard for last cycle (avoid calling work on an empty queue)
        if ( jobQueue.Size() > 0 && jobQueue.Front()->rr_timeRemaining > 0 )
        {
            jobQueue.Front()->Work( RR );
            timer++;  
            cycles++; // each cycle is one unit of time; keep track of total time/cycles
        }

    }

    out_stream << endl;

    // Summary Stats: out_stream
    rrStats( out_stream, allJobs, cycles, timePerProcess );

    // Summary Stats: cout
    rrStats( cout, allJobs, cycles, timePerProcess );

    // Close the file
    out_stream.close();
}



// ============= GK: Helper Functions =================================

void Processor::fcfsStats( ostream& out, vector<Job>& allJobs, int cycles )
{
    int numJobs = static_cast<int>( allJobs.size() );
    int runningTotal(0);

    out << "--+------------------------------------------+-------------------" << endl
        << "  |  First Come First Served (FCFS) Results  |"   << endl
        << "  +------------------------------------------+"   << endl << endl
        << setw(7)  << right << "JOB ID"
        << setw(18) << right << "TIME TO COMPLETE" << endl; 

    for ( int i = 0; i < numJobs; i++ )
    {
        out << setw(6)  << right << allJobs[i].id              << "\t " 
            << setw(15) << right << allJobs[i].fcfs_finishTime << endl;

        runningTotal += allJobs[i].fcfs_finishTime;
    }

    out << endl 
        << "Total Time to Process: ............... " << cycles << endl
        << " (time for all jobs to complete\n  processing)" << endl << endl
        << "Average Processing Time: ............. " 
        << ( runningTotal / numJobs ) << "." << ( runningTotal % numJobs ) << endl
        << " (average time to complete, including\n  wait time of jobs in queue)" << endl << endl;
    
    out << "=================================================================" << endl << endl;
}

void Processor::rrStats( ostream& out, vector<Job>& allJobs, int cycles, int timePerProcess )
{
    int numJobs = static_cast<int>( allJobs.size() );
    int runningTotal(0);

    out << "--+----------------------------+---------------------------------" << endl
        << "  |  Round Robin (RR) Results  |"   << endl
        << "  +----------------------------+"   << endl << endl
        << setw(7)  << right << "JOB ID"
        << setw(18) << right << "TIME TO COMPLETE"
        << setw(19) << right << "TIMES INTERRUPTED" << endl; 

    for ( int i = 0; i < numJobs; i++ )
    {
        out << setw(6)  << right << allJobs[i].id                  << "\t " 
            << setw(15) << right << allJobs[i].rr_finishTime       << "\t"
            << setw(11) << right << allJobs[i].rr_timesInterrupted << endl;

        runningTotal += allJobs[i].rr_finishTime;
    }

    out << endl
        << "Total Time to Process: ............... " << cycles << endl
        << " (time for all jobs to complete\n  processing)" << endl << endl
        << "Average Processing Time: ............. " 
        << ( runningTotal / numJobs ) << "." << ( runningTotal % numJobs ) << endl
        << " (average time to complete, including\n  wait time of jobs in queue)" << endl << endl
        << "Round Robin Interval ................. " << timePerProcess << endl
        << " (time per processing interval; every\n  n units, rotate jobs)" << endl << endl;

    
    out << "=================================================================" << endl << endl;
}


#endif
