#ifndef QUEUE_HPP
#define QUEUE_HPP

#include "LinkedList.hpp"

template <typename T>
class Queue
{
    public:
    void Push( T data )
    {
        m_list.PushBack( data );
        // throw runtime_error( "not yet implemented" ); // placeholder
        return;
    }

    void Pop()
    {
        m_list.PopFront();
        // throw runtime_error( "not yet implemented" ); // placeholder
        return;
    }

    T& Front()
    {
        // throw runtime_error( "not yet implemented" ); // placeholder
        return m_list.GetFirst();
    }

    int Size()
    {
        // throw runtime_error( "not yet implemented" ); // placeholder
        return m_list.Size();
    }

    private:

    LinkedList<T> m_list;
};

#endif
