#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <fstream>
#include <iomanip>
#include <stdexcept>
using namespace std;

#include "Job.hpp"
#include "Processor.hpp"
#include "Menu.hpp"

vector<Job> CreateJobs( int jobCount )
{
    vector<Job> jobList;

    for ( int i = 0; i < jobCount; i++ )
    {
        Job job;

        job.fcfs_timeRemaining = rand() % 100 + 50;
        job.rr_timeRemaining = job.fcfs_timeRemaining;

        job.id = i;
        jobList.push_back( job );

        cout << " Job "    << setw(4) << i 
             << ", fcfs: " << setw(3) << job.fcfs_timeRemaining 
             << ", rr: "   << setw(3) << job.rr_timeRemaining << endl;
    }

    return jobList;
}

void FillJobQueues( Queue<Job*>& fcfsQueue, Queue<Job*>& rrQueue, vector<Job>& allJobs )
{
    for ( unsigned int i = 0; i < allJobs.size(); i++ )
    {
        fcfsQueue.Push( &allJobs[i] );  // OJO: & = Address-of operator
        rrQueue.Push( &allJobs[i] );    // going into a queue of pointers
    }
}

int main()
{
    srand( time( NULL ) );
    string fcfs_file = "gk_result-fcfs.txt";
    string rr_file   = "gk_result-rr.txt";

    // Get config info
    Menu::Header( "Job Processor" );
    int totalJobs = Menu::GetValidChoice( 10, 9999, "How many jobs? (10-9999)" );
    int roundRobin = Menu::GetValidChoice( 1, 10, "Round Robin time interval? (1-10) " );

    // Create jobs...
    cout << endl << "Creating jobs..." << endl;
    vector<Job> allJobs = CreateJobs( totalJobs );

    // ...and copy those jobs into each queue (each queue gets all the jobs)
    // FCFS = First Come, First Served  |  RR = Round Robbin
    Queue<Job*> fcfs, rr;
    cout << endl << "Filling queues..." << endl;
    FillJobQueues( fcfs, rr, allJobs );

    // Process each queue with the different types of scheduling
    Processor cpu;
    cout << endl << "Processing with FCFS..." << endl;
    try
    {
        cpu.FirstComeFirstServe( allJobs, fcfs, fcfs_file );
    }
    catch ( runtime_error& e )
    {
        cout << "ERROR: " << e.what() << endl;
    }

    cout << endl << "Processing with RR..." << endl;
    try
    {
        cpu.RoundRobin( allJobs, rr, roundRobin, rr_file );
    }
    catch ( runtime_error& e )
    {
        cout << "ERROR: " << e.what() << endl;
    }

    cout << "  +----------------------------------+" << endl
         << "  | Output Files                     |" << endl
         << "  +----------------------------------+" << endl
         << "  | FCFS:         " << fcfs_file << " |" << endl
         << "  | Round Robin:  " << rr_file << "   |" <<  endl
         << "  +----------------------------------+" << endl<< endl
         << "DONE" << endl << endl;

    // cin.ignore();
    // cin.get();

    return 0;
}
