#include <iostream>
#include "LinkedList.hpp"
using namespace std;

int main()
{
    cout << endl << "---------- LinkedList ----------" << endl;

    cout << endl << "TEST: Instantiation" << endl;
    LinkedList<int> testList;
    testList.printList();
    
    cout << endl << "TEST: GetFront() on empty list" << endl
         << " testList.GetFront(): ";
    if ( testList.GetFront() != NULL )
        cout << *testList.GetFront() << endl;
    else
         cout << "NULL" << endl;
    
    cout << endl << "TEST: GetBack() on empty list" << endl
         << " testList.GetBack(): ";
    if ( testList.GetBack() != NULL )
        cout << *testList.GetBack() << endl;
    else
         cout << "NULL" << endl;
    
    cout << endl << "TEST: PushBack()" << endl
         << " testList.PushBack(101): ";
    if ( testList.PushBack(101) ) cout << "Pass" << endl;
    else                          cout << "Fail" << endl;
    cout << " testList.PushBack(102): ";
    if ( testList.PushBack(102) ) cout << "Pass" << endl;
    else                          cout << "Fail" << endl;
    cout << " testList.PushBack(103): ";
    if ( testList.PushBack(103) ) cout << "Pass" << endl;
    else                          cout << "Fail" << endl;
    testList.printList();

    cout << endl << "TEST: PushFront()" << endl;
    cout << " testList.PushFront(777): ";
    if ( testList.PushFront(777) ) cout << "Pass" << endl;
    else                           cout << "Fail" << endl;
    cout << " testList.PushFront(888): ";
    if ( testList.PushFront(888) ) cout << "Pass" << endl;
    cout << " testList.PushFront(999): ";
    if ( testList.PushFront(999) ) cout << "Pass" << endl;
    testList.printList();

    cout << endl << "TEST: PopBack()" << endl;
    cout << " testList.PopBack(): ";
    if ( testList.PopBack() ) cout << "Pass" << endl;
    else                      cout << "Fail" << endl;
    testList.printList();
    
    cout << endl << "TEST: PopFront()" << endl;
    cout << " testList.PopFront(): ";
    if ( testList.PopFront() ) cout << "Pass" << endl;
    else                       cout << "Fail" << endl;
    testList.printList();
    
    cout << endl << "TEST: GetAtIndex()" << endl
         << " testList.GetAtIndex(1): " << testList.GetAtIndex(1)->data << endl
         << " testList.GetAtIndex(3): " << testList.GetAtIndex(3)->data << endl
         << " testList.GetAtIndex(0): " << testList.GetAtIndex(0)->data << endl
         << " testList.GetAtIndex(2): " << testList.GetAtIndex(2)->data << endl;

    cout << endl << "TEST: InsertAtIndex()" << endl;
    cout << " testList.InsertAtIndex(1, 11): ";
    if ( testList.InsertAtIndex(1, 11) ) cout << "Pass" << endl;
    else                                 cout << "Fail" << endl;
    testList.printList();
    cout << " testList.InsertAtIndex(0, 22): ";
    if ( testList.InsertAtIndex(0, 22) ) cout << "Pass" << endl;
    else                                 cout << "Fail" << endl;
    testList.printList();
    cout << " testList.InsertAtIndex(4, 33): ";
    if ( testList.InsertAtIndex(4, 33) ) cout << "Pass" << endl;
    else                                 cout << "Fail" << endl;
    testList.printList();
    cout << " testList.InsertAtIndex(6, 66): ";
    if ( testList.InsertAtIndex(6, 66) ) cout << "Pass" << endl;
    else                                 cout << "Fail" << endl;
    testList.printList();

    cout << endl << "TEST: GetFront()" << endl
         << " testList.GetFront(): ";
    if ( testList.GetFront() != NULL )  cout << *testList.GetFront() << endl;
    else                                cout << "NULL" << endl;
    testList.printList();
    
    cout << endl << "TEST: GetBack()" << endl
         << " testList.GetBack(): ";
    if ( testList.GetBack() != NULL ) cout << *testList.GetBack() << endl;
    else                              cout << "NULL" << endl;
    testList.printList();

    cout << endl << "TEST: RemoveAtIndex()" << endl;
    cout << " testList.RemoveAtIndex(0): " << endl;
    cout << "  List Before:";
    testList.printList();
    cout << "  Remove at index[0]: " << testList.GetAtIndex(0)->data;
    if ( testList.RemoveAtIndex(0) )    cout << " (Pass)" << endl;
    else                                cout << " (Fail)" << endl;
    cout << "  List After: ";
    testList.printList();
    cout << endl;

// --

    cout << " testList.RemoveAtIndex(6): ";
    if ( testList.RemoveAtIndex(6) )    cout << "Pass" << endl;
    else                                cout << "Fail" << endl;
    testList.printList();
    cout << " testList.RemoveAtIndex(4): ";
    if ( testList.RemoveAtIndex(4) )    cout << "Pass" << endl;
    else                                cout << "Fail" << endl;
    testList.printList();
    cout << " testList.RemoveAtIndex(2): ";
    if ( testList.RemoveAtIndex(2) )    cout << "Pass" << endl;
    else                                cout << "Fail" << endl;
    testList.printList();



    cout << endl << "TEST: Destructor (calls Clear):";

    return 0;
}
