#ifndef LINKEDLIST_HPP
#define LINKEDLIST_HPP

#include <iostream>
#include "Node.hpp"
using namespace std;

template<typename T>
class LinkedList
{
private:
    Node<T>* m_ptrFirst;
    Node<T>* m_ptrLast;
    int      m_itemCount;

public:
    LinkedList();
    virtual ~LinkedList();

    bool PushFront( const T& newItem );
    bool PushBack( const T& newItem );
    bool InsertAtIndex( int atIndex, const T& item );

    bool PopFront();
    bool PopBack();
    bool RemoveAtIndex( int atIndex );
    Node<T>* GetAtIndex( int atIndex );
    T* GetFront();
    T* GetBack();
    int Size();
    void printList();
    void Clear();

};

    LinkedList() : m_ptrFirst(nullptr), m_ptrLast(nullptr), m_itemCount(0)
    {
        // Intentionally left blank
    }

    virtual ~LinkedList()
    {
        Clear();
    }

    bool PushBack( const T& newItem )
    {
        // create & setup a new Node
        Node<T>* newNodePtr = new Node<T>;  // create a new Node
        newNodePtr->data = newItem;         // set new Node's data
        newNodePtr->ptrPrev = nullptr; 
        newNodePtr->ptrNext = nullptr;

        // ADJUST POINTERS...
        //  1. empty LinkedList
        if ( m_itemCount == 0 )
        {
            m_ptrFirst = newNodePtr;            // Sets the LinkedList values
            m_ptrLast  = newNodePtr;            // Sets the LinkedList values
            m_itemCount++;
            return true;
        }

        //  2. LinkedList with item(s): new Node is new-last Node
        m_ptrLast->ptrNext = newNodePtr; // old-last points forward to new-last
        newNodePtr->ptrPrev = m_ptrLast; // new-last points back to old-last
        m_ptrLast = newNodePtr;          // LinkedList Last points to new-last

        m_itemCount++;
        return true;
    }

    bool PushFront( const T& newItem )
    {
        // create & setup a new Node
        Node<T>* newNodePtr = new Node<T>;  // create a new Node
        newNodePtr->data = newItem;         // set new Node's data
        newNodePtr->ptrPrev = nullptr; 
        newNodePtr->ptrNext = nullptr;

        // ADJUST POINTERS...
        //  1. empty LinkedList
        if ( m_itemCount == 0 )
        {
            m_ptrFirst = newNodePtr;            // Sets the LinkedList values
            m_ptrLast  = newNodePtr;            // Sets the LinkedList values
            m_itemCount++;
            return true;
        }

        //  2. LinkedList with item(s): new Node is new-last Node
        newNodePtr->ptrNext = m_ptrFirst;        // new-first points to old-first
        m_ptrFirst->ptrPrev = newNodePtr;       // old-first points back to new-first
        m_ptrFirst = newNodePtr;                // LinkedList first is new Node
    
        m_itemCount++;
        return true;
    }

    bool PopBack()
    {
        Node<T>* deleteNodePtr = m_ptrLast; // node to delete
        m_ptrLast = m_ptrLast->ptrPrev;     // new lastNode
        m_ptrLast->ptrNext = nullptr;       // set lastNode ptrNext to nullptr

        deleteNodePtr->ptrPrev = nullptr;
        deleteNodePtr->ptrNext = nullptr;
        delete deleteNodePtr;
        
        m_itemCount--;
        return true;
    }

    bool PopFront()
    {
        Node<T>* deleteNodePtr = m_ptrFirst; // node to delete
        m_ptrFirst = m_ptrFirst->ptrNext;    // new firstNode
        m_ptrFirst->ptrPrev = nullptr;       // set firstNode ptrPrev to nullptr

        deleteNodePtr->ptrPrev = nullptr;
        deleteNodePtr->ptrNext = nullptr;
        delete deleteNodePtr;
        
        m_itemCount--;
        return true;
    }

    Node<T>* GetAtIndex( int atIndex )
    {
        /* ========= NEED TO THROW EXCEPTIONS FOR THIS TO WORK =========
        if ( (atIndex < 0) || (atIndex > m_itemCount) ) // bounds check
            return false;

        else if ( m_ptrLast == nullptr )                // empty list check
            return false;
        ============================================================ */
        int counter(0);
        Node<T>* curPtr = m_ptrFirst;

        while ( counter != atIndex )
        {
            curPtr = curPtr->ptrNext;
            counter++;
        }
        return curPtr;
    }

    bool InsertAtIndex( int atIndex, const T& item )
    {
        if ( (atIndex < 0) || (atIndex > m_itemCount) )  // bounds check
            return false;

        else if ( m_itemCount == 0 )                     // empty list
        {
            PushBack(item);
            return true;
        }

        else if ( atIndex == 0 )                         // special case
        {
            PushFront( item);
            return true;
        }
        else
        {
            // set up the new node to insert
            Node<T>* newNode = new Node<T>;
            newNode->data = item;
            newNode->ptrPrev = nullptr;
            newNode->ptrNext = nullptr;

            // cutPtr == node at atIndex
            Node<T>* curPtr = GetAtIndex( atIndex );

            // adjust new node pointers first
            newNode->ptrNext = curPtr;
            newNode->ptrPrev = curPtr->ptrPrev;

            // adjust node before new node (points to new node)
            Node<T>* beforeNewNodePtr = curPtr->ptrPrev;
            beforeNewNodePtr->ptrNext = newNode;

            // adjust curPtr
            curPtr->ptrPrev = newNode;

            m_itemCount++;

            return true;
        }
     
        return true; // placeholder / STUB   
    }

    bool RemoveAtIndex( int atIndex )
    {
        if ( (atIndex < 0) || (atIndex > m_itemCount) )  // bounds check
            return false;

        else if ( m_itemCount == 0 )                     // empty list
        {
            return false;
        }

        else if ( atIndex == 0 )                         // remove first
        {
            PopFront();
            return true;
        }
        else if ( atIndex = ( m_itemCount - 1 ) )       // remove last
        {
            PopBack();
            return true;
        }
        else
        {
            Node<T>* nodeRemove = GetAtIndex( atIndex );
            Node<T>* nodeBefore = nodeRemove->ptrPrev;
            Node<T>* nodeAfter  = nodeRemove->ptrNext;

            nodeBefore->ptrNext = nodeAfter;
            nodeAfter->ptrPrev  = nodeBefore;

            nodeRemove->ptrPrev = nullptr;
            nodeRemove->ptrNext = nullptr;
            delete nodeRemove;
            
            m_itemCount--;
            return true;
        }

    }


/* ----------------------------- NEEDS HELP ---------------------------
-------------------------------------------------------------------- */
    T* GetFront()                    // return pointer to return NULL
    {
        if ( m_ptrFirst != nullptr)  // if there is a first node
            return &m_ptrFirst->data;
        else                         // LinkedList is empty
            return NULL;
    }

    T* GetBack()
    {
        if ( m_ptrLast != nullptr)  // if there is at least one node
            return &m_ptrLast->data;
        else                        // LinkedList is empty
            return NULL; // ??? WHAT DO WE RETURN HERE ????
    }

    int Size()
    {
        return m_itemCount;
    }

    void printList()
    {
        Node<T>* curPtr = m_ptrFirst;
        cout << " | LinkedList Data (size " << Size() << "): "; 
        while ( (m_ptrLast != nullptr) && (curPtr != nullptr) )
        {
            cout << curPtr->data << " ";
            curPtr = curPtr->ptrNext; // advance to the next Node
        }
        cout << endl; 
    }

    void Clear()    
    {
        cout << endl << " Clear()" << " | Size: " << Size() << endl;
        Node<T>* curPtr = m_ptrFirst;
        Node<T>* nodeToDelete = nullptr;

        while ( (m_ptrLast != nullptr) && (curPtr != nullptr) )
        {
            cout << "  Delete Node with data: " << curPtr->data << endl;
            nodeToDelete = curPtr;    // set to delete the current Node
            curPtr = curPtr->ptrNext; // advance to the next Node

            nodeToDelete->ptrPrev = nullptr;
            nodeToDelete->ptrNext = nullptr;
            delete nodeToDelete;
        }
        m_itemCount = 0;
        cout << " Size: " << Size() << endl << endl;
    }

#endif
