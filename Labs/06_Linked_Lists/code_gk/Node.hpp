// Ch 4 - Linked List

#ifndef NODE_HPP
#define NODE_HPP

template<typename T>
struct Node
{
    Node();

    Node<T>* ptrPrev;
    Node<T>* ptrNext;

    T data;
};

template<typename T>
Node<T>::Node() : ptrPrev(nullptr), ptrNext(nullptr)
{
    // intentionally left blank
}

#endif
