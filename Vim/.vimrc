"" Greg's crappy .vimrc file
"" Ideas: http://vim.wikia.com/wiki/Example_vimrc

" --- GENERAL STUFF ---
filetype plugin on      " endable filetype detection
syntax enable           " use syntax color highlighting
set wildmenu            " better command-line completion
set showcmd             " show partial commands in the last line of the screen
set number              " display line numbers on the left
set nocompatible        " use vim defaults.
set title               " show title of file in vim title bar
set history=1000        " save the last 1000 lines

" disable the automatic insertion of a comment character on ENTER:
autocmd BufNewFile,BufRead * setlocal formatoptions-=r
" disable the automatic insertion of a comment character on o or O:
autocmd BufNewFile,BufRead * setlocal formatoptions-=o


" --- LINE INDENTING ---
set smarttab            " enable smart handling of the tab key
set expandtab           " tabs are converted to spaces
set shiftwidth=4        " numbers of spaces to (auto)indent
set tabstop=4           " numbers of spaces for the tab character
set scrolloff=3         " keep 3 lines when scrolling


" --- ABBREVIATIONS --
"  This is gets me the less-than sign by typeing ';;'-
" ab ;; <
"  This is gets me the greater-than sign by typeing '::'-
" ab :: >

" Change the color scheme
set t_Co=256
colorscheme gman1
"colorscheme desert
