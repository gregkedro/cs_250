#include <iostream>
#include <fstream>
#include <string>
#include <cctype>
#include <cstdlib>
#include <vector>
using namespace std;

// Function to validate names: alpha, space, hyphen only.
bool nameValidation( string name);

int main()
{
    int choice(1);
    string input;  // for name from user input
    string name;   // for name from file
    vector<string> fileNames;
    
    // If there is a current names.txt file, display the names
    ifstream in_stream("names.txt");
    if ( in_stream )
    {
        while ( ! in_stream.eof() )
        {
            getline(in_stream, name);
            fileNames.push_back(name);
        }
        cout << "\n------------------------------\n"
             << "\nHere are the names currently in names.txt:\n";
        for ( int i = 0; i < (int)fileNames.size(); i++)
        {
            cout << " " << fileNames[i] << endl;
        }
        in_stream.close();
    }

    // Set up the output stream to names.txt (appending)
    ofstream out_stream("names.txt", ios::app);  
    if ( !out_stream )
    {
        cout << "\nOutput File Stream Connection Failed. Exiting...\n\n";
        exit(1);
    }
    
    while ( choice != 2 )
    {
        cout << "------------------------------\n\n"
             << "1. Enter Full Name     2. Quit\n\n"
             << "Your choice (1 or 2): ";

        // Validate User Input (should be int: 1 or 2)
        while(1)
        {
            if ( cin >> choice &&
               ( choice == 1   ||
                 choice == 2    )  )  break;
            else
            { 
                cout << "\nINVALID INPUT: Please input a 1 or a 2: ";
                cin.clear(); // clears the error flag on cin
                cin.ignore(1000, '\n');
            }
        }

        if ( choice != 2 )
        {
            cout << "Full Name: ";
            cin.ignore(1000,'\n');
            getline(cin,input);

            // Validation of Input Name
            if ( nameValidation(input) )
            {
                out_stream << input << "\n";
                cout << " => \"" << input << "\" has been added to names.txt.\n";
            }
            else
            {
                cout << "\nInvalid Input (names must be alpha, space, or hyphen only).\n"
                     << "* NAME NOT ADDED TO FILE *\n";
            }
        }
        cout << endl;
    }

    out_stream.close();
    
    // Display the names now in the file
    cout << "Here are the names now currently in names.txt:\n";
    in_stream.open("names.txt");
    if ( !in_stream )
    {
        cout << "\nInput File Stream Connection Failed. Exiting...\n\n";
        exit(1);
    }
    while ( ! in_stream.eof() )
    {
        getline(in_stream, name);
        cout << " " << name << endl;
    }

    in_stream.close();

    cout << "Goodbye\n\n";
    return 0;
}


bool nameValidation( string name)
{
    bool valid(true);
    int len = name.length();
 
    while ( len > 0 && valid == true )
    {
        if    ( ( isalpha(name[len-1]) )  ||
                ( isspace(name[len-1]) )  || 
                ( name[len-1] == '-'   )   )    valid = true;
        else                                    valid = false;
        len--;
    }
   
    return valid;    
}
