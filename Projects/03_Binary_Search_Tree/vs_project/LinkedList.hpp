#ifndef LINKED_LIST_HPP
#define LINKED_LIST_HPP

#include <iostream>
using namespace std;

#include "Node.hpp"

template <typename TK, typename TD>
class LinkedList
{
    public:
    LinkedList()
    {
        ptrFront = nullptr;
        ptrEnd = nullptr;
        m_size = 0;
    }

    ~LinkedList()
    {
        Clear();
    }

    void Clear()
    {
        while ( ptrFront != nullptr )
        {
            PopBack();
        }
    }

    void PushBack( const TK& newKey, const TD& newData )
    {
        Node<TK, TD>* newNode = new Node<TK, TD>;
        newNode->data = newData;
        newNode->key = newKey;
        newNode->IsNotTree();

        if ( ptrFront == nullptr && ptrEnd == nullptr )
        {
            ptrFront = newNode;
            ptrEnd = newNode;
        }
        else
        {
            // New node goes at end of list
            newNode->ptrLeft = ptrEnd;
            ptrEnd->ptrRight = newNode;
            ptrEnd = newNode;
        }

        m_size++;
    }

    void PushFront( const TK& newKey, const TD& newData )
    {
        Node<TK, TD>* newNode = new Node<TK, TD>;
        newNode->data = newData;
        newNode->key = newKey;
        newNode->IsNotTree();

        if ( ptrFront == nullptr && ptrEnd == nullptr )
        {
            ptrFront = newNode;
            ptrEnd = newNode;
        }
        else
        {
            // New node goes at the front of the list
            newNode->ptrRight = ptrFront;
            ptrFront->ptrLeft = newNode;
            ptrFront = newNode;
        }

        m_size++;
    }

    void PopBack()
    {
        if ( m_size == 0 )
        {
            return;
        }
        else if ( ptrFront == ptrEnd )
        {
            delete ptrFront;
            ptrFront = nullptr;
            ptrEnd = nullptr;
            m_size = 0;
        }
        else
        {
            // Remove the last element
            Node<TK, TD>* penultimate = ptrEnd->ptrLeft;
            delete ptrEnd;
            ptrEnd = penultimate;
            ptrEnd->ptrRight = nullptr;
            m_size--;
        }
    }

    void PopFront()
    {
        if ( m_size == 0 )
        {
            return;
        }
        else if ( ptrFront == ptrEnd )
        {
            delete ptrFront;
            ptrFront = nullptr;
            ptrEnd = nullptr;
            m_size = 0;
        }
        else
        {
            // Remove the first element
            Node<TK, TD>* next = ptrFront->ptrRight;
            delete ptrFront;
            ptrFront = next;
            ptrFront->ptrLeft = nullptr;
            m_size--;
        }
    }

    void Remove( TK& key )
    {
        int counter = 0;
        Node<TK, TD>* ptrCurrent = ptrFront;

        while (ptrCurrent != nullptr)
        {
            if (ptrCurrent->key == key)
            {
                break;
            }
            ptrCurrent = ptrCurrent->ptrRight;
            counter++;
        }

        if (ptrCurrent != nullptr)
        {
            // Only one node
            if (ptrFront == ptrEnd)
            {
                ptrFront = nullptr;
                ptrEnd = nullptr;
                m_size = 0;
            }
            // Item to remove is first in list
            else if (counter == 0)
            {
                PopFront();
            }
            // Item to remove is last in list
            else if (counter == (m_size - 1))
            {
                PopBack();
            }
            // Item to remove is in multiple-item list
            else
            {
                Node<TK, TD>* ptrLeft = ptrCurrent->ptrLeft;
                Node<TK, TD>* ptrRight = ptrCurrent->ptrRight;

                // delete ptrCurrent;

                if (ptrLeft != nullptr)
                {
                    ptrLeft->ptrRight = ptrRight;
                }

                if (ptrRight != nullptr)
                {
                    ptrRight->ptrLeft = ptrLeft;
                }

                delete ptrCurrent;
                ptrCurrent = nullptr;

                m_size--;
            }
        }
    }

    TD& GetFirst()
    {
        return ptrFront->data;
    }

    TD& GetLast()
    {
        return ptrEnd->data;
    }

    TD& GetItemAtPosition( int index )
    {
        int counter = 0;
        Node<TK, TD>* ptrCurrent = ptrFront;

        while ( counter < index )
        {
            ptrCurrent = ptrCurrent->ptrRight;
            counter++;
        }

        return ptrCurrent->data;
    }

    TD* GetItemWithKey( const TK& key )
    {
        int counter = 0;
        Node<TK, TD>* ptrCurrent = ptrFront;

        while ( counter < Size() )
        {
            if ( ptrCurrent->key == key )
            {
                break;
            }
            ptrCurrent = ptrCurrent->ptrRight;
            counter++;
        }

        if ( ptrCurrent == nullptr )
        {
            return nullptr;
        }

        return &ptrCurrent->data;
    }

    bool Contains( TD item )
    {
        Node<TK, TD>* ptrCurrent = ptrFront;

        while ( ptrCurrent != nullptr )
        {
            if ( ptrCurrent->data == item )
            {
                return true;
            }

            ptrCurrent = ptrCurrent->ptrRight;
        }

        return false;
    }

    int Size()
    {
        return m_size;
    }

    bool IsEmpty()
    {
        return ( m_size == 0 );
    }

    void Display()
    {
        Node<TK, TD>* ptrCurrent = ptrFront;
        int counter = 0;
        while ( ptrCurrent != nullptr )
        {
            cout << counter++ << ": " 
                 << ptrCurrent->data.make 
                 << ", " << ptrCurrent->data.model
                 << " (" << ptrCurrent->data.price << ")" << endl;
            ptrCurrent = ptrCurrent->ptrRight;
        }
    }

    private:
    Node<TK, TD>* ptrFront;
    Node<TK, TD>* ptrEnd;
    int m_size;
};

#endif




