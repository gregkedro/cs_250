// CS-250 Project #1: Writing Tests and Linear Structures
// Gregory A. Kedrovsky

#include <iostream>
#include <string>
#include "Tester.hpp"

using namespace std;

int main()
{
    Tester tester;
    tester.RunTests();
    cout << endl << endl;

    return 0;
}


