#ifndef _LIST_HPP
#define _LIST_HPP

/* ----- Testing Purposes Only ------
#include <iostream>
using namespace std;
---------------------------------- */

const int ARRAY_SIZE = 100;

template <typename T>
class List
{
private:
    // private member variables
    int m_itemCount;
    T m_arr[ARRAY_SIZE];

    // functions for interal-workings
    bool ShiftRight( int atIndex )
    {
        // NOTE: m_itemCount handled by calling function
        if      ( m_itemCount == 0 )          return true;
        else if ( m_itemCount == ARRAY_SIZE ) return false;
        else 
        {
            bool retVal(true);
            for ( int i = m_itemCount; i > atIndex; i-- )
            {
                m_arr[i] = m_arr[i-1];
                retVal = ( m_arr[i] == m_arr[i-1] );
            }
            return retVal;
        }
    }

    bool ShiftLeft( int atIndex )
    {
        // NOTE: m_itemCount handled by this function
        bool retVal;

        if       ( m_itemCount == 0 )       retVal = false;
        else if  ( atIndex >= m_itemCount ) retVal = false;
        else if  ( m_itemCount == 1 )
        {
            m_itemCount--;
            retVal = true;
        }
        else
        {
            /* ---------------- Testing Purposes Only -----------------
            cout << "m_itemCount = " << m_itemCount << endl;
            cout << "LIST: ";
            for ( int i = 0; i < m_itemCount; i++ )
                cout << m_arr[i] << "  "; 
            cout << endl;
            -------------------------------------------------------- */

            for ( int i = atIndex; i < ( m_itemCount - 1 ); i++ )
            {
                m_arr[i] = m_arr[i+1]; 
                retVal = ( m_arr[i] == m_arr[i+1] );
            }
            m_itemCount--;
            retVal = true;

            /* ---------------- Testing Purposes Only -----------------
            cout << "LIST: ";
            for ( int i = 0; i < m_itemCount; i++ )
                cout << m_arr[i] << "  ";
            cout << endl << "m_itemCount = " << m_itemCount << endl << endl;
            -------------------------------------------------------- */
        }
        return retVal;
    }

public:
    List() : m_itemCount(0) {}

    ~List() {}

    // Core functionality
    int Size() const
    {
        return m_itemCount;
    }

    bool IsEmpty() const
    {
        if ( m_itemCount == 0 ) return true;
        else                    return false;
    }

    bool IsFull() const
    {
        if ( m_itemCount == ARRAY_SIZE ) return true;
        else                             return false;
    }

    bool PushFront( const T& newItem )
    {
        if ( m_itemCount == ARRAY_SIZE )    return false;
        
        bool shiftOK( ShiftRight(0) );
        m_arr[0] = newItem;
        m_itemCount++;
        if ( shiftOK && m_arr[0] == newItem )
            return true;
    }

    bool PushBack( const T& newItem )
    {
        if ( IsFull() )   return false;
        
        m_arr[m_itemCount] = newItem;
        m_itemCount++;
        return true;
    }

    bool Insert( int atIndex, const T& item )
    {
        if ( m_itemCount == ARRAY_SIZE )      // sorry, we're full
            return false;

        else if ( atIndex > m_itemCount ||    // invalid index
                  atIndex < 0             )
            return false;

        else if ( atIndex == m_itemCount )    // insert at back;
            return ( PushBack(item) ); 

        else if ( atIndex == 0 )              // insert at front;
            return ( PushFront(item) );

        else                                  // insert & shift;
        {
            ShiftRight(atIndex);
            m_arr[atIndex] = item;
            m_itemCount++;
            return true;
        }
    }

    bool PopFront()
    {
        return ( ShiftLeft(0) );
    }

    bool PopBack()
    {
        if ( m_itemCount == 0 ) return false;

        m_itemCount--; 
        return true;
    }

    bool RemoveItem( const T& item )
    {
        bool retVal(false);
        int  counter(0);

        while ( counter < m_itemCount ) 
        {
            if ( *Get(counter) == item )
            {
                RemoveIndex(counter); // the shiftLeft requires that the 
                counter--;            // same index be checked again
                retVal = true;
            }
            counter++;
        }
        return retVal;
    }

    bool RemoveIndex( int atIndex )
    {
        if ( m_itemCount == 0 )              // empty array
            return false;
        else if ( atIndex < 0 ||
                  atIndex >= m_itemCount )  // out of bounds
            return false;
        else                                // valid index
            return ShiftLeft(atIndex);
    }

    void Clear()
    {
        m_itemCount = 0;
    }

    // Accessors
    T* Get( int atIndex )
    {
        if ( atIndex < m_itemCount )
            return &m_arr[atIndex];
        else
            return nullptr;
    }

    T* GetFront()
    {
        if ( m_itemCount == 0 ) return nullptr;
        return &m_arr[0];
    }

    T* GetBack()
    {
        if ( m_itemCount == 0 ) return nullptr;
        return &m_arr[ m_itemCount - 1 ];
    }

    // Additional functionality
    int GetCountOf( const T& item ) const
    {
        int count(0);
        for ( int i = 0; i < m_itemCount; i++ )
        {
            if ( m_arr[i] == item )  count++;
        }
        return count;
    }

    bool Contains( const T& item ) const
    {
        bool retVal(false);
        for ( int i = 0; i < m_itemCount; i++ )
            if ( m_arr[i] == item ) retVal = true;
        return retVal;
    }

    friend class Tester;
};

#endif
