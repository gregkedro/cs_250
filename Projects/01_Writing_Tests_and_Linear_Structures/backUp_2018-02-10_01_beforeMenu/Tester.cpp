#include "Tester.hpp"

void Tester::RunTests()
{
    Test_Init();
    Test_IsEmpty();
    Test_IsFull();
    Test_Size();
    Test_GetCountOf();
    Test_Contains();

    Test_PushFront();
    Test_PushBack();

    Test_Get();
    Test_GetFront();
    Test_GetBack();

    Test_PopFront();
    Test_PopBack();
    Test_Clear();
    Test_ShiftRight();
    Test_ShiftLeft();
    Test_RemoveItem();
    Test_RemoveIndex();
    Test_Insert();
}

void Tester::DrawLine()
{
    cout << endl;
    for ( int i = 0; i < 72; i++ )
    {
        cout << "-";
    }
    cout << endl;
}

void Tester::Test_Init()
{
    DrawLine();
    cout << "TEST: Test_Init (assuming test of the constructor)" << endl;

    {   // Test #1: Create a List, do nothing
        cout << endl << "Test 1: Create a List, do nothing" << endl;
        List<int> testList;

        bool expectedResult = true;
        bool actualResult;

        cout << " Size: " << testList.Size() << endl;
        cout << " IsEmpty: " << testList.IsEmpty() << " (1 == true)" << endl;
        if ( testList.IsEmpty()   &&                      // List should exit, be empty
             testList.Size() == 0  ) actualResult = true; // and return 0 for size
        else                         actualResult = false;

        cout << " Expected Result: " << expectedResult << " (1 == true)" << endl;
        cout << " Actual Result:   " << actualResult   << endl;

        if ( actualResult ) cout << " Pass" << endl;
        else                cout << " Fail" << endl;
    }   // Test #1 end

    {   // Test #2: Do not create a list
        cout << endl << "Test 2: Do not create a list" << endl;

        cout << " Nothing to test because nothing was instantiated" << endl;
        cout << " Pass (always)" << endl;

    }   // Test #2 end

    /* ------------------- Testing Purposes Only -----------------------
    {
        cout << "CHECK" << endl;
        List<string> gArray;
        for ( int i = 0; i < 10; i++ )
        {
            cout << i << " ->  " << "word" << endl;
            gArray.PushBack("word");
        }

        cout << endl << "Size: " << gArray.Size() << endl;
        for ( int i = 0; i < 11; i++ )
        {
            if ( gArray.Get(i) == nullptr )
                cout << i << "  => nullptr" << endl;
            else
                cout << i << "  => " << *gArray.Get(i) << endl;
        }
    }
    ----------------------------------------------------------------- */

}

void Tester::Test_ShiftRight()
{
    DrawLine();
    cout << "TEST: Test_ShiftRight" << endl;
    
    {   // Test #1: PushFront() calls ShiftRight(). Create int List, PushFront 10 items
        cout << endl << "Test #1: PushFront() calls ShiftRight(). "
                     << "Create int List, PushFront 10 items" << endl;
        List<int> testList;

        cout << " start Size():     " << testList.Size() << endl;

        bool resultExpected(true);
        bool resultActual;
        for ( int i = 0; i < 10; i++ )
            resultActual = testList.PushFront(i+1);

        cout << " end Size():      " << testList.Size() << endl;

        /* ------------------- Testing Purposes Only -------------------
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  testList[" << i << "] => " << *testList.Get(i) << "\t";
            if ( (i + 1) % 3 == 0 ) cout << endl;
        }        
        ---------------------------------------------------------------- */

        cout << " GetFront():      "  << *testList.GetFront() << endl
             << " GetBack():        " << *testList.GetBack() <<  endl
             << " Expected Result:  " << resultExpected << " (1 == true)" << endl
             << " Actual Result:    " << resultActual << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;

    }   // Test #1 end

    {   // Test #2: PushFront() calls ShiftRight(). Create int List, PushFront 101 items
        cout << endl << "Test #2: PushFront() calls ShiftRight(). "
                     << "Create int List, PushFront 101 items" << endl;
        List<int> testList;

        cout << " start Size():      " << testList.Size() << endl;

        bool resultExpected(false);
        bool resultActual;
        for ( int i = 0; i <= 100; i++ )
            resultActual = testList.PushFront(i+1);

        cout << " end Size():      " << testList.Size() << endl;

        cout << " Expected Result:   " << resultExpected << " (0 == false)" << endl
             << " Actual Result:     " << resultActual << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;

    }   // Test #2 end

    {   // Test #3: PushFront() calls ShiftRight(). Create string List, PushFront 5 items.
        cout << endl << "Test #3: PushFront() callss ShiftRight(). "
             << "Create string List, PushFront 5 items." << endl;
        List<string> testList;

        cout << " Expected Result: " << endl
             << "  algo1" << endl
             << "  algo2, algo1" << endl
             << "  algo3, algo2, algo1" << endl
             << "  algo4, algo3, algo2, algo1" << endl
             << "  algo5, algo4, algo3, algo2, algo1" << endl;

        cout << " Actual Result: Five calls to PushFront() showing ShiftRight()" << endl;
        for ( int i = 0; i < 5; i++ )
        {
            cout << "  ";
            testList.PushFront( "algo" + to_string(i+1) );
            for ( int j = 0; j < testList.Size(); j++ )
            {
                if ( j < 4 ) cout << *testList.Get(j) << ", ";
                else         cout << *testList.Get(j);
            }
            cout << endl;
        }
        if ( *testList.Get(0) == "algo5" &&
             *testList.Get(1) == "algo4" &&
             *testList.Get(2) == "algo3" &&
             *testList.Get(3) == "algo2" &&
             *testList.Get(4) == "algo1"  ) cout << " Pass" << endl;
        else                                cout << " Fail" << endl;
    }   //Test #3 end

    {   // Test #4: Insert() calls ShiftRight(). Create string List of 5 elements, Insert() 2
        cout << endl << "Test #4: Insert() calls ShiftRight(). "
             << "Create string List of 5 elements, Insert() 2" << endl;
        List<string> testList;
        bool pass;

        for ( int i = 0; i < 5; i++ )
            testList.PushBack( "a" + to_string(i+1) );

        cout << " Start List:       (size: " << testList.Size() << "): ";
        for ( int i = 0; i < testList.Size(); i++ )
        {
            if ( i < (testList.Size()-1)) cout << *testList.Get(i) << ", ";
            else                          cout << *testList.Get(i) << endl;
        }

        testList.Insert(3, "INS1");
        cout << " First Insert:" << endl
             << "  Expected Result: (size: 6): a1, a2, a3, INS1, a4, a5" << endl
             << "  Actual Result:   (size: " << testList.Size() << "): ";
        for ( int i = 0; i < testList.Size(); i++ )
        {
            if ( i < (testList.Size()-1)) cout << *testList.Get(i) << ", ";
            else                          cout << *testList.Get(i) << endl;
        }
        if ( *testList.Get(0) == "a1"   &&
             *testList.Get(1) == "a2"   &&
             *testList.Get(2) == "a3"   &&
             *testList.Get(3) == "INS1" &&
             *testList.Get(4) == "a4"   &&
             *testList.Get(5) == "a5"   &&
              testList.Size() ==   6   ) { 
                                           cout << "  Pass" << endl;
                                           pass = true; 
                                         }
        else                             {
                                           cout << "  Fail" << endl;
                                           pass = false;
                                         }

        
        testList.Insert(2, "INS2");
        cout << " Second Insert:" << endl
             << "  Expected Result: (size: 7): a1, a2, INS2, a3, INS1, a4, a5" << endl
             << "  Actual Result:   (size: " << testList.Size() << "): ";
        for ( int i = 0; i < testList.Size(); i++ )
        {
            if ( i < (testList.Size()-1)) cout << *testList.Get(i) << ", ";
            else                          cout << *testList.Get(i) << endl;
        }
        if ( *testList.Get(0) == "a1"   &&
             *testList.Get(1) == "a2"   &&
             *testList.Get(2) == "INS2" &&
             *testList.Get(3) == "a3"   &&
             *testList.Get(4) == "INS1" &&
             *testList.Get(5) == "a4"   &&
             *testList.Get(6) == "a5"   &&
              testList.Size() ==   7   ) { 
                                           cout << "  Pass" << endl;
                                           pass = true; 
                                         }
        else                             {
                                           cout << "  Fail" << endl;
                                           pass = false;
                                         }

        cout << " Overall Test Result:" << endl;
        if ( pass ) cout << "  Pass" << endl;
        else        cout << "  Fail" << endl;

    }   // Test #4 end


}

void Tester::Test_ShiftLeft()
{
    DrawLine();
    cout << "TEST: Test_ShiftLeft" << endl << endl
         << "[1] PopFront() Calls ShiftLeft(0): Test #1 & Test #2" << endl;

    {   // Test #1: Create int List, PushBack 10 items, PopFront() 3 times
        cout << endl << "Test #1: Create int List, PushBack() 10 items, PopFront() 3 times" << endl;
        List<int> testList;

        for ( int i = 0; i < 10; i++ )
            testList.PushBack(i+1);
        
        cout << " Start Size: " << testList.Size() << endl;
        cout << " Start List: ";
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << *testList.Get(i);
            if ( i < testList.Size()-1 )    cout << ", ";
            else            cout << endl;
        }

        bool resultExpected = true;
        bool resultActual;
        bool pass;

        cout << " Expected Result 1st PopFront(): " << resultExpected << " (1 == true)" << endl;

        resultActual = testList.PopFront();

        cout << "   Actual Result 1st PopFront(): " << resultActual;
                    if ( resultExpected == resultActual )
                    {   
                        cout << " (Pass)" << endl;
                        pass = true;
                    }
                    else
                    {
                        cout << " (Fail)" << endl;
                        pass = false;
                    }
        cout << "   Size: " << testList.Size() << endl;
        cout << "   List: ";
                    for ( int i = 0; i < testList.Size(); i++ )
                    {
                        cout << *testList.Get(i);
                        if ( i < testList.Size()-1 )    cout << ", ";
                        else            cout << endl;
                    }

        cout << " Expected Result 2nd PopFront(): " << resultExpected << " (1 == true)" << endl;

        resultActual = testList.PopFront();

        cout << "   Actual Result 2nd PopFront(): " << resultActual;
                    if ( resultExpected == resultActual )
                    {   
                        cout << " (Pass)" << endl;
                        pass = true;
                    }
                    else
                    {
                        cout << " (Fail)" << endl;
                        pass = false;
                    }
        cout << "   Size: " << testList.Size() << endl;
        cout << "   List: ";
                    for ( int i = 0; i < testList.Size(); i++ )
                    {
                        cout << *testList.Get(i);
                        if ( i < testList.Size()-1 )    cout << ", ";
                        else            cout << endl;
                    }

        cout << " Expected Result 3rd PopFront(): " << resultExpected << " (1 == true)" << endl;

        resultActual = testList.PopFront();

        cout << "   Actual Result 2nd PopFront(): " << resultActual;
                    if ( resultExpected == resultActual )
                    {   
                        cout << " (Pass)" << endl;
                        pass = true;
                    }
                    else
                    {
                        cout << " (Fail)" << endl;
                        pass = false;
                    }
        cout << "   Size: " << testList.Size() << endl;
        cout << "   List: ";
                    for ( int i = 0; i < testList.Size(); i++ )
                    {
                        cout << *testList.Get(i);
                        if ( i < testList.Size()-1 )    cout << ", ";
                        else            cout << endl;
                    }

        cout << " End Size: " <<  testList.Size() << " (should be 7)" << endl
             << " Get(0):   " << *testList.Get(0) << " (should be 4)" << endl;

        cout << " Overall Test Result:" << endl;
        if ( pass ) cout << "   Pass" << endl;
        else        cout << "   Fail" << endl;

    }   // Test #1 end

    {   // Test #2: Create int List, PushBack() 2 items, PopFront() 3 times
        cout << endl << "Test #2: Create int List, PushBack() 2 items, PopFront() 3 times" << endl;
        List<int> testList;

        testList.PushBack(1);
        testList.PushBack(2);
        
        cout << " Start Size: " << testList.Size() << endl;
        cout << " Start List: ";
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << *testList.Get(i);
            if ( i < testList.Size()-1 )    cout << ", ";
            else            cout << endl;
        }

        bool resultExpected = true;
        bool resultActual;
        bool pass;

        cout << " Expected Result 1st PopFront(): " << resultExpected << " (1 == true)" << endl;

        resultActual = testList.PopFront();

        cout << "   Actual Result 1st PopFront(): " << resultActual;
                    if ( resultExpected == resultActual )
                    {
                        cout << " (Pass)" << endl;
                        pass = true;
                    }
                    else
                    {
                        cout << " (Fail)" << endl;
                        pass = false;
                    }
        cout << "   Size: " << testList.Size() << endl;
        cout << "   List: ";
                    if ( testList.Size() > 0 )
                        for ( int i = 0; i < testList.Size(); i++ )
                        {
                            cout << *testList.Get(i);
                            if ( i < testList.Size()-1 )    cout << ", ";
                            else            cout << endl;
                        }
                    else    cout << "(empty array)" << endl;

        cout << " Expected Result 2nd PopFront(): " << resultExpected << " (1 == true)" << endl;

        resultActual = testList.PopFront();

        cout << "   Actual Result 2nd PopFront(): " << resultActual;
                    if ( resultExpected == resultActual )
                    {
                        cout << " (Pass)" << endl;
                        pass = true;
                    }
                    else
                    {
                        cout << " (Fail)" << endl;
                        pass = false;
                    }
        cout << "   Size: " << testList.Size() << endl;
        cout << "   List: ";
                    if ( testList.Size() > 0 )
                        for ( int i = 0; i < testList.Size(); i++ )
                        {
                            cout << *testList.Get(i);
                            if ( i < testList.Size()-1 )    cout << ", ";
                            else            cout << endl;
                        }
                    else    cout << "(empty array)" << endl;

        resultExpected = false;

        cout << " Expected Result 3rd PopFront(): " << resultExpected << " (0 == false)" << endl;

        resultActual = testList.PopFront();

        cout << "   Actual Result 3rd PopFront(): " << resultActual;
                    if ( resultExpected == resultActual )
                    {
                        cout << " (Pass)" << endl;
                        pass = true;
                    }
                    else
                    {
                        cout << " (Fail)" << endl;
                        pass = false;
                    }
        cout << "   Size: " << testList.Size() << endl;
        cout << "   List: ";
                    if ( testList.Size() > 0 )
                        for ( int i = 0; i < testList.Size(); i++ )
                        {
                            cout << *testList.Get(i);
                            if ( i < testList.Size()-1 )    cout << ", ";
                            else            cout << endl;
                        }
                    else    cout << "(empty array)" << endl;

        cout << " End Size: " <<  testList.Size() << " (should be 0)" << endl;

        cout << " Overall Test Result:" << endl;
        if ( pass ) cout << "   Pass" << endl;
        else        cout << "   Fail" << endl;

    }   // Test #2 end

    cout << endl << "[2] RemoveIndex() Calls ShiftLeft(n): Test #3 & Test #4" << endl;

    {   // Test #3: Create int List, PushBack() 10 items, RemoveIndex(3) 3 times
        cout << endl << "Test #3: Create int List, PushBack() 10 itmes, RemoveIndex(3) 3 times" << endl;
        List<int> testList;

        for ( int i = 0; i < 10; i++ )
            testList.PushBack(i+1);
        
        cout << " Start Size: " << testList.Size() << endl;
        cout << " Start List: ";
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << *testList.Get(i);
            if ( i < testList.Size()-1 )    cout << ", ";
            else            cout << endl;
        }

        bool resultExpected = true;
        bool resultActual;
        bool pass;

        cout << " Expected Result 1st RemoveIndex(3): " << resultExpected << " (1 == true)" << endl;

        resultActual = testList.RemoveIndex(3);

        cout << "   Actual Result 1st RemoveIndex(3): " << resultActual;
                    if ( resultExpected == resultActual )
                    {   
                        cout << " (Pass)" << endl;
                        pass = true;
                    }
                    else
                    {
                        cout << " (Fail)" << endl;
                        pass = false;
                    }
        cout << "   Size:   " <<  testList.Size() << endl
             << "   Get(2): " << *testList.Get(2) << endl
             << "   Get(3): " << *testList.Get(3) << endl;
        cout << "   List: ";
                    for ( int i = 0; i < testList.Size(); i++ )
                    {
                        cout << *testList.Get(i);
                        if ( i < testList.Size()-1 ) cout << ", ";
                        else                         cout << endl;
                    }

        cout << " Expected Result 2nd RemoveIndex(3): " << resultExpected << " (1 == true)" << endl;

        resultActual = testList.RemoveIndex(3);

        cout << "   Actual Result 2nd RemoveIndex(3): " << resultActual;
                    if ( resultExpected == resultActual )
                    {   
                        cout << " (Pass)" << endl;
                        pass = true;
                    }
                    else
                    {
                        cout << " (Fail)" << endl;
                        pass = false;
                    }
        cout << "   Size:   " <<  testList.Size() << endl
             << "   Get(2): " << *testList.Get(2) << endl
             << "   Get(3): " << *testList.Get(3) << endl;
        cout << "   List: ";
                    for ( int i = 0; i < testList.Size(); i++ )
                    {
                        cout << *testList.Get(i);
                        if ( i < testList.Size()-1 )    cout << ", ";
                        else            cout << endl;
                    }

        cout << " Expected Result 3rd RemoveIndex(3): " << resultExpected << " (1 == true)" << endl;

        resultActual = testList.RemoveIndex(3);

        cout << "   Actual Result 3rd RemoveIndex(3): " << resultActual;
                    if ( resultExpected == resultActual )
                    {   
                        cout << " (Pass)" << endl;
                        pass = true;
                    }
                    else
                    {
                        cout << " (Fail)" << endl;
                        pass = false;
                    }
        cout << "   Size:   " <<  testList.Size() << endl
             << "   Get(2): " << *testList.Get(2) << endl
             << "   Get(3): " << *testList.Get(3) << endl;
        cout << "   List: ";
                    for ( int i = 0; i < testList.Size(); i++ )
                    {
                        cout << *testList.Get(i);
                        if ( i < testList.Size()-1 )    cout << ", ";
                        else            cout << endl;
                    }

        cout << " End Size: " <<  testList.Size() << " (should be 7)" << endl
             << " Get(2):   " << *testList.Get(0) << " (should be 3)" << endl
             << " Get(3):   " << *testList.Get(0) << " (should be 7)" << endl;

        cout << " Overall Test Result:" << endl;
        if ( pass ) cout << "   Pass" << endl;
        else        cout << "   Fail" << endl;

    }   // Test #3 end

    {   // Test #4: Create int List, PushBack() 10 items, RemoveIndex(10) 1 time
        cout << endl << "Test #4: Create int List, PushBack() 10 items, RemoveIndex(10) 1 time" << endl;
        List<int> testList;

        for ( int i = 0; i < 10; i++ )
            testList.PushBack(i+1);
        
        cout << " Start Size: " << testList.Size() << endl;
        cout << " Start List: ";
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << *testList.Get(i);
            if ( i < testList.Size()-1 )    cout << ", ";
            else            cout << endl;
        }

        bool resultExpected = false;
        bool resultActual;
        bool pass;

        cout << " Expected Result RemoveIndex(10): " << resultExpected << " (0 == false)" << endl;

        resultActual = testList.RemoveIndex(10);

        cout << "   Actual Result RemoveIndex(10): " << resultActual;
                    if ( resultExpected == resultActual )
                    {   
                        cout << " (Pass)" << endl;
                        pass = true;
                    }
                    else
                    {
                        cout << " (Fail)" << endl;
                        pass = false;
                    }
        cout << "   Size:  " <<  testList.Size() << endl
             << "   List:  ";
                    for ( int i = 0; i < testList.Size(); i++ )
                    {
                        cout << *testList.Get(i);
                        if ( i < testList.Size()-1 ) cout << ", ";
                        else                         cout << endl;
                    }

    }   // Test #4 end

    {   // Test #5: Create int List, leave it empty, RemoveIndex(0), RemoveIndex(0)
        cout << endl << "Test #5: Create int List, leave it empty, RemoveIndex(0), RemoveIndex(0)" << endl;
        List<int> testList;

        cout << " Start Size: " << testList.Size() << endl;

        bool resultExpected = false;
        bool resultActual;
        bool pass;

        cout << " Expected Result RemoveIndex(0): " << resultExpected << " (0 == false)" << endl;
        resultActual = testList.RemoveIndex(0);
        cout << "   Actual Result RemoveIndex(0): " << resultActual;
                    if ( resultExpected == resultActual )   cout << " (Pass)" << endl;
                    else                                    cout << " (Fail)" << endl;

        cout << " Expected Result RemoveIndex(1): " << resultExpected << " (0 == false)" << endl;
        resultActual = testList.RemoveIndex(1);
        cout << "   Actual Result RemoveIndex(1): " << resultActual;
                    if ( resultExpected == resultActual )   cout << " (Pass)" << endl;
                    else                                    cout << " (Fail)" << endl;

        cout << " Size:  " <<  testList.Size() << endl;

    }   // Test #5 end



}

void Tester::Test_Size()
{
    DrawLine();
    cout << "TEST: Test_Size" << endl;

    {   // Test #1: Create a List, do nothing
        cout << endl << "Test 1: Create a List, do nothing" << endl;
        List<int> testList;

        int expectedSize = 0;
        int actualSize   = testList.Size();

        cout << " Expected size: " << expectedSize << endl;
        cout << " Actual size:   " << actualSize   << endl;

        if ( actualSize == expectedSize )   cout << " Pass" << endl;
        else                                cout << " Fail" << endl;
    }   // Test #1 end

    {   // Test #2: Create a List, insert 1 item
        cout << endl << "Test 2: Create a List, insert 1 item" << endl;
        List<int> testList;

        testList.PushBack( 1 );

        int expectedSize = 1;
        int actualSize   = testList.Size();

        cout << " Expected size: " << expectedSize << endl;
        cout << " Actual size:   " << actualSize   << endl;

        if ( actualSize == expectedSize )   cout << " Pass" << endl;
        else                                cout << " Fail" << endl;
    }   // Test #2 end

    {   // Test #3: Create a List, fill all elements
        cout << endl << "Test 3: Create a List, fill all elements" << endl;
        List<int> testList;

        for ( int i = 0; i < 100; i++ )
            testList.PushBack( i+1 );

        int expectedSize = 100;
        int actualSize   = testList.Size();

        cout << " Expected size: " << expectedSize << endl;
        cout << " Actual size:   " << actualSize   << endl;

        if ( actualSize == expectedSize )   cout << " Pass" << endl;
        else                                cout << " Fail" << endl;
    }   // Test #3 end

    {   // Test #4: Create a List, fill all elements less 1
        cout << endl << "Test 4: Create a List, fill all elements less 1" << endl;
        List<int> testList;

        for ( int i = 0; i < 99; i++ )
            testList.PushBack( i+1 );

        int expectedSize = 99;
        int actualSize   = testList.Size();

        cout << " Expected size: " << expectedSize << endl;
        cout << " Actual size:   " << actualSize   << endl;

        if ( actualSize == expectedSize )   cout << " Pass" << endl;
        else                                cout << " Fail" << endl;
    }   // Test #4 end

    {   // Test #5: Create a List, insert 110 items 
        cout << endl << "Test 5: Create a List, insert 110 items" << endl;
        List<int> testList;

        for ( int i = 0; i < 110; i++ )
            testList.PushBack( i+1 );

        int expectedSize = 100;
        int actualSize   = testList.Size();

        cout << " Expected size: " << expectedSize << endl;
        cout << " Actual size:   " << actualSize   << endl;

        if ( actualSize == expectedSize )   cout << " Pass" << endl;
        else                                cout << " Fail" << endl;
    }   // Test #4 end

}

void Tester::Test_IsEmpty()
{
    DrawLine();
    cout << "TEST: Test_IsEmpty" << endl;

    {   // Test #1: Create a List, do nothing
        cout << endl << "Test 1: Create a List, do nothing" << endl;
        List<int> testList;

        cout << " Size: " << testList.Size() << endl
             << " IsEmpty(): " << endl
             << "  Expected Result: 1 (true)" << endl
             << "  Actual Result:   " << testList.IsEmpty() << endl;

        if ( testList.IsEmpty() )   cout << " Pass" << endl; 
        else                        cout << " Fail" << endl;
    }   // Test #1 end

    {   // Test #2: Create a List, insert 1 item
        cout << endl << "Test 2: Create a List, insert 1 item" << endl;
        List<int> testList;
        
        testList.PushBack( 1 );

        cout << " Size: " << testList.Size() << endl
             << " IsEmpty(): " << endl
             << "  Expected Result: 0 (false)" << endl
             << "  Actual Result:   " << testList.IsEmpty() << endl;

        if ( !testList.IsEmpty() )  cout << " Pass" << endl; 
        else                        cout << " Fail" << endl;
    }   // Test #2 end

    {   // Test #3: Create a List, insert 99 items
        cout << endl << "Test 2: Create a List, insert 99 items" << endl;
        List<int> testList;
        
        for ( int i = 0; i < 99; i++ )
            testList.PushBack( i + 11 );

        cout << " Size: " << testList.Size() << endl
             << " IsEmpty(): " << endl
             << "  Expected Result: 0 (false)" << endl
             << "  Actual Result:   " << testList.IsEmpty() << endl;

        if ( !testList.IsEmpty() )  cout << " Pass" << endl; 
        else                        cout << " Fail" << endl;
    }   // Test #3 end

    {   // Test #4: Create a List, insert 100 items
        cout << endl << "Test 2: Create a List, insert 100 items" << endl;
        List<int> testList;
        
        for ( int i = 0; i < 100; i++ )
            testList.PushBack( i + 11 );

        cout << " Size: " << testList.Size() << endl
             << " IsEmpty(): " << endl
             << "  Expected Result: 0 (false)" << endl
             << "  Actual Result:   " << testList.IsEmpty() << endl;

        if ( !testList.IsEmpty() )  cout << " Pass" << endl; 
        else                        cout << " Fail" << endl;
    }   // Test #4 end

    {   // Test #5: Create a List, insert 110 items
        cout << endl << "Test 2: Create a List, insert 110 items" << endl;
        List<int> testList;
        
        for ( int i = 0; i < 110; i++ )
            testList.PushBack( i + 11 );

        cout << " Size: " << testList.Size() << endl
             << " IsEmpty(): " << endl
             << "  Expected Result: 0 (false)" << endl
             << "  Actual Result:   " << testList.IsEmpty() << endl;

        if ( !testList.IsEmpty() )  cout << " Pass" << endl; 
        else                        cout << " Fail" << endl;
    }   // Test #5 end
}

void Tester::Test_IsFull()
{
    DrawLine();
    cout << "TEST: Test_IsFull" << endl;

    {   // Test #1: Create a List, do nothing
        cout << endl << "Test 1: Create a List, do nothing" << endl;
        List<int> testList;

        cout << " Size: " << testList.Size() << endl
             << " IsFull(): " << endl
             << "  Expected Result: 0 (false)" << endl
             << "  Actual Result:   " << testList.IsFull() << endl;

        if ( !testList.IsFull() )   cout << " Pass" << endl; 
        else                        cout << " Fail" << endl;
    }   // Test #1 end

    {   // Test #2: Create a List, insert 1 item
        cout << endl << "Test 2: Create a List, insert 1 item" << endl;
        List<int> testList;
        
        testList.PushBack( 1 );

        cout << " Size: " << testList.Size() << endl
             << " IsFull(): " << endl
             << "  Expected Result: 0 (false)" << endl
             << "  Actual Result:   " << testList.IsFull() << endl;

        if ( !testList.IsFull() )   cout << " Pass" << endl; 
        else                        cout << " Fail" << endl;
    }   // Test #2 end

    {   // Test #3: Create a List, fill all elements
        cout << endl << "Test 3: Create a List, fill all elements" << endl;
        List<int> testList;

        for ( int i = 0; i < 100; i++ )
            testList.PushBack( i + 1 );

        cout << " Size: " << testList.Size() << endl
             << " IsFull(): " << endl
             << "  Expected Result: 1 (true)" << endl
             << "  Actual Result:   " << testList.IsFull() << endl;

        if ( testList.IsFull() )    cout << " Pass" << endl; 
        else                        cout << " Fail" << endl;
    }   // Test #3 end

    {   // Test #4: Create a List, fill all elements less 1
        cout << endl << "Test 4: Create a List, fill all elements less 1" << endl;
        List<int> testList;
        
        for ( int i = 0; i < 99; i++ )
            testList.PushBack( i + 1 );

        cout << " Size: " << testList.Size() << endl
             << " IsFull(): " << endl
             << "  Expected Result: 0 (false)" << endl
             << "  Actual Result:   " << testList.IsFull() << endl;

        if ( !testList.IsFull() )   cout << " Pass" << endl; 
        else                        cout << " Fail" << endl;
    }   // Test #4 end

    {   // Test #5: Create a List, insert 101 items
        cout << endl << "Test 5: Create a List, insert 101 items" << endl;
        List<int> testList;
        
        for ( int i = 0; i < 101; i++ )
            testList.PushBack( i + 1 );

        cout << " Size: " << testList.Size() << endl
             << " IsFull(): " << endl
             << "  Expected Result: 1 (true)" << endl
             << "  Actual Result:   " << testList.IsFull() << endl;

        if ( testList.IsFull() )   cout << " Pass" << endl; 
        else                        cout << " Fail" << endl;
    }   // Test #5 end
}

void Tester::Test_PushFront()
{
    DrawLine();
    cout << "TEST: Test_PushFront" << endl;

    {   // Test #1: Create an int List, PushFront() 1 item
        cout << endl << "Test #1: Create an int List, PushFront() 1 item" << endl;
        List<int> testList;
        bool resultBool;
        int  resultSize;

        resultBool = testList.PushFront(1);
        resultSize = testList.Size();

        cout << " Expected Result: 1 (true)" << endl
             << "  Actual Result:  "         << resultBool << endl
             << " Expected Size:   1"        << endl
             << "  Actual Size:    "         << resultSize << endl
             << " GetFront():      " << *testList.GetFront() << endl
             << " GetBack():       " << *testList.GetBack() << endl;

        if ( (resultBool) && ( resultSize == 1 ) )  cout << " Pass" << endl;
        else                                        cout << " Fail" << endl;

    }   // Test #1 end

    {   // Test #2: Create an int List, PushFront() 2 items"
        cout << endl << "Test #2: Create an int List, PushFront() 2 items" << endl;
        List<int> testList;
        bool resultBool;
        int  resultSize;

        resultBool = testList.PushFront(1);
        resultBool = testList.PushFront(2);
        resultSize = testList.Size();

        cout << " Expected Result: 1 (true)" << endl
             << "  Actual Result:  "         << resultBool << endl
             << " Expected Size:   2"        << endl
             << "  Actual Size:    "         << resultSize << endl
             << " GetFront():      " << *testList.GetFront() << endl
             << " GetBack():       " << *testList.GetBack() << endl;

        if ( (resultBool) && ( resultSize == 2 ) )  cout << " Pass" << endl;
        else                                        cout << " Fail" << endl;

        /* ------------------- Testing Purposes Only -------------------
        cout << " LIST: ";
        for ( int i = 0; i < 2; i++ )
        {
            cout << *testList.Get(i);
            if ( i < 2-1 ) cout << ", ";
        }
        cout << endl;
        ---------------------------------------------------------------- */

    }   // Test #2 end

    {   // Test #3: Create an int List, PushFront() 5 items"
        cout << endl << "Test #3: Create an int List, PushFront() 5 items" << endl;
        List<int> testList;
        bool resultBool;
        int  resultSize;

        for ( int i = 1; i <= 5; i++ )
            resultBool = testList.PushFront(i);
        resultSize = testList.Size();

        cout << " Expected Result: 1 (true)" << endl
             << "  Actual Result:  "         << resultBool << endl
             << " Expected Size:   5"        << endl
             << "  Actual Size:    "         << resultSize << endl
             << " GetFront():      " << *testList.GetFront() << endl
             << " GetBack():       " << *testList.GetBack() << endl;

        cout << " GetFront() + PopFront(): ";
        for (int i = 0; i < 5; i++ )
        {
            cout << *testList.GetFront();
            testList.PopFront();
            if ( i < 5-1 ) cout << ", ";
        }
        cout << endl;

        if ( (resultBool) && ( resultSize == 5 ) )  cout << " Pass" << endl;
        else                                        cout << " Fail" << endl;

        /* ------------------- Testing Purposes Only -------------------
        cout << " LIST: ";
        for ( int i = 0; i < 5; i++ )
        {
            cout << *testList.Get(i);
            if ( i < 5-1 ) cout << ", ";
        }
        cout << endl;
        ---------------------------------------------------------------- */

    }   // Test #3 end

    {   // Test #4: Create an int List, PushFront() 99 items"
        cout << endl << "Test #4: Create an int List, PushFront() 99 items" << endl;
        List<int> testList;
        bool resultBool;
        int  resultSize;

        for ( int i = 1; i <= 99; i++ )
            resultBool = testList.PushFront(i);
        resultSize = testList.Size();

        cout << " Expected Result:  1 (true)" << endl
             << "  Actual Result:   "         << resultBool << endl
             << " Expected Size:   99"        << endl
             << "  Actual Size:    "         << resultSize << endl
             << " GetFront():      " << *testList.GetFront() << endl
             << " GetBack():        " << *testList.GetBack() << endl;

        if ( (resultBool) && ( resultSize == 99 ) )  cout << " Pass" << endl;
        else                                        cout << " Fail" << endl;

        /* ------------------- Testing Purposes Only -------------------
        cout << " LIST: ";
        for ( int i = 0; i < 99; i++ )
        {
            cout << *testList.Get(i);
            if ( i < 99-1 ) cout << ", ";
        }
        cout << endl;
        ---------------------------------------------------------------- */

    }   // Test #4 end

    {   // Test #5: Create an int List, PushFront() 100 items"
        cout << endl << "Test #5: Create an int List, PushFront() 100 items" << endl;
        List<int> testList;
        bool resultBool;
        int  resultSize;

        for ( int i = 1; i <= 100; i++ )
            resultBool = testList.PushFront(i);
        resultSize = testList.Size();

        cout << " Expected Result:   1 (true)" << endl
             << "  Actual Result:    "         << resultBool << endl
             << " Expected Size:   100"        << endl
             << "  Actual Size:    "         << resultSize << endl
             << " GetFront():      " << *testList.GetFront() << endl
             << " GetBack():         " << *testList.GetBack() << endl;

        if ( (resultBool) && ( resultSize == 100 ) )  cout << " Pass" << endl;
        else                                        cout << " Fail" << endl;

        /* ------------------- Testing Purposes Only -------------------
        cout << " LIST: ";
        for ( int i = 0; i < 100; i++ )
        {
            cout << *testList.Get(i);
            if ( i < 100-1 ) cout << ", ";
        }
        cout << endl;
        ---------------------------------------------------------------- */

    }   // Test #5 end

    {   // Test #6: Create an int List, PushFront() 110 items"
        cout << endl << "Test #6: Create an int List, PushFront() 110 items" << endl;
        List<int> testList;
        bool resultBool;
        int  resultSize;

        for ( int i = 1; i <= 110; i++ )
            resultBool = testList.PushFront(i);
        resultSize = testList.Size();

        cout << " Expected Result:   0 (false)" << endl
             << "  Actual Result:    "         << resultBool << endl
             << " Expected Size:   100"        << endl
             << "  Actual Size:    "         << resultSize << endl
             << " GetFront():      " << *testList.GetFront() << endl
             << " GetBack():         " << *testList.GetBack() << endl;

        if ( (!resultBool) && ( resultSize == 100 ) )  cout << " Pass" << endl;
        else                                        cout << " Fail" << endl;

        /* ------------------- Testing Purposes Only -------------------
        cout << " LIST: ";
        for ( int i = 0; i < 100; i++ )
        {
            cout << *testList.Get(i);
            if ( i < 100-1 ) cout << ", ";
        }
        cout << endl;
        ---------------------------------------------------------------- */

    }   // Test #6 end

    {   // Test #7: Create a string List, PushFront() 110 items"
        cout << endl << "Test #7: Create an string List, PushFront() 5 items" << endl;
        List<string> testList;
        bool resultBool;
        int  resultSize;

        resultBool = testList.PushFront("A");
        resultBool = testList.PushFront("B");
        resultBool = testList.PushFront("C");
        resultBool = testList.PushFront("D");
        resultBool = testList.PushFront("E");
        resultSize = testList.Size();

        cout << " Expected Result:  1 (true)" << endl
             << "  Actual Result:   "         << resultBool << endl
             << " Expected Size:    5"        << endl
             << "  Actual Size:     "         << resultSize << endl
             << " GetFront():       " << *testList.GetFront() << endl
             << " GetBack():        " << *testList.GetBack() << endl;

        cout << " GetFront() + PopFront(): ";
        for (int i = 0; i < 5; i++ )
        {
            cout << *testList.GetFront();
            testList.PopFront();
            if ( i < 5-1 ) cout << ", ";
        }
        cout << endl;


        if ( (resultBool) && ( resultSize == 5 ) )  cout << " Pass" << endl;
        else                                         cout << " Fail" << endl;

        /* ------------------- Testing Purposes Only -------------------
        cout << " LIST: ";
        for ( int i = 0; i < 5; i++ )
        {
            cout << *testList.Get(i);
            if ( i < 5-1 ) cout << ", ";
        }
        cout << endl;
        ---------------------------------------------------------------- */

    }   // Test #7 end

}

void Tester::Test_PushBack()
{
    DrawLine();
    cout << "TEST: Test_PushBack" << endl;

    {   // Test #1: Create an int List, PushBack() 1 item
        cout << endl << "Test #1: Create an int List, PushBack() 1 item" << endl;
        List<int> testList;
        bool resultBool;
        int  resultSize;

        resultBool = testList.PushBack(1);
        resultSize = testList.Size();

        cout << " Expected Result: 1 (true)" << endl
             << "  Actual Result:  "         << resultBool << endl
             << " Expected Size:   1"        << endl
             << "  Actual Size:    "         << resultSize << endl
             << " GetFront():      " << *testList.GetFront() << endl
             << " GetBack():       " << *testList.GetBack() << endl;

        if ( (resultBool) && ( resultSize == 1 ) )  cout << " Pass" << endl;
        else                                        cout << " Fail" << endl;

    }   // Test #1 end

    {   // Test #2: Create an int List, PushBack() 2 items"
        cout << endl << "Test #2: Create an int List, PushBack() 2 items" << endl;
        List<int> testList;
        bool resultBool;
        int  resultSize;

        resultBool = testList.PushBack(1);
        resultBool = testList.PushBack(2);
        resultSize = testList.Size();

        cout << " Expected Result: 1 (true)" << endl
             << "  Actual Result:  "         << resultBool << endl
             << " Expected Size:   2"        << endl
             << "  Actual Size:    "         << resultSize << endl
             << " GetFront():      " << *testList.GetFront() << endl
             << " GetBack():       " << *testList.GetBack() << endl;

        if ( (resultBool) && ( resultSize == 2 ) )  cout << " Pass" << endl;
        else                                        cout << " Fail" << endl;

    }   // Test #2 end

    {   // Test #3: Create an int List, PushBack() 5 items"
        cout << endl << "Test #3: Create an int List, PushBack() 5 items" << endl;
        List<int> testList;
        bool resultBool;
        int  resultSize;

        for ( int i = 1; i <= 5; i++ )
            resultBool = testList.PushBack(i);
        resultSize = testList.Size();

        cout << " Expected Result: 1 (true)" << endl
             << "  Actual Result:  "         << resultBool << endl
             << " Expected Size:   5"        << endl
             << "  Actual Size:    "         << resultSize << endl
             << " GetFront():      " << *testList.GetFront() << endl
             << " GetBack():       " << *testList.GetBack() << endl;

        cout << " GetFront() + PopFront(): ";
        for (int i = 0; i < 5; i++ )
        {
            cout << *testList.GetFront();
            testList.PopFront();
            if ( i < 5-1 ) cout << ", ";
        }
        cout << endl;

        if ( (resultBool) && ( resultSize == 5 ) )  cout << " Pass" << endl;
        else                                        cout << " Fail" << endl;

    }   // Test #3 end

    {   // Test #4: Create an int List, PushBack() 99 items"
        cout << endl << "Test #4: Create an int List, PushBack() 99 items" << endl;
        List<int> testList;
        bool resultBool;
        int  resultSize;

        for ( int i = 1; i <= 99; i++ )
            resultBool = testList.PushBack(i);
        resultSize = testList.Size();

        cout << " Expected Result:  1 (true)" << endl
             << "  Actual Result:   "         << resultBool << endl
             << " Expected Size:   99"        << endl
             << "  Actual Size:    "         << resultSize << endl
             << " GetFront():       " << *testList.GetFront() << endl
             << " GetBack():       " << *testList.GetBack() << endl;

        if ( (resultBool) && ( resultSize == 99 ) )  cout << " Pass" << endl;
        else                                        cout << " Fail" << endl;

    }   // Test #4 end

    {   // Test #5: Create an int List, PushBack() 100 items"
        cout << endl << "Test #5: Create an int List, PushBack() 100 items" << endl;
        List<int> testList;
        bool resultBool;
        int  resultSize;

        for ( int i = 1; i <= 100; i++ )
            resultBool = testList.PushBack(i);
        resultSize = testList.Size();

        cout << " Expected Result:   1 (true)" << endl
             << "  Actual Result:    "         << resultBool << endl
             << " Expected Size:   100"        << endl
             << "  Actual Size:    "         << resultSize << endl
             << " GetFront():        " << *testList.GetFront() << endl
             << " GetBack():       " << *testList.GetBack() << endl;

        if ( (resultBool) && ( resultSize == 100 ) )  cout << " Pass" << endl;
        else                                        cout << " Fail" << endl;

    }   // Test #5 end

    {   // Test #6: Create an int List, PushBack() 110 items"
        cout << endl << "Test #6: Create an int List, PushBack() 110 items" << endl;
        List<int> testList;
        bool resultBool;
        int  resultSize;

        for ( int i = 1; i <= 110; i++ )
            resultBool = testList.PushBack(i);
        resultSize = testList.Size();

        cout << " Expected Result:   0 (false)" << endl
             << "  Actual Result:    "         << resultBool << endl
             << " Expected Size:   100"        << endl
             << "  Actual Size:    "         << resultSize << endl
             << " GetFront():        " << *testList.GetFront() << endl
             << " GetBack():       " << *testList.GetBack() << endl;

        if ( (!resultBool) && ( resultSize == 100 ) )  cout << " Pass" << endl;
        else                                        cout << " Fail" << endl;

    }   // Test #6 end

    {   // Test #7: Create a string List, PushBack() 110 items"
        cout << endl << "Test #7: Create an string List, PushBack() 5 items" << endl;
        List<string> testList;
        bool resultBool;
        int  resultSize;

        resultBool = testList.PushBack("A");
        resultBool = testList.PushBack("B");
        resultBool = testList.PushBack("C");
        resultBool = testList.PushBack("D");
        resultBool = testList.PushBack("E");
        resultSize = testList.Size();

        cout << " Expected Result:  1 (true)" << endl
             << "  Actual Result:   "         << resultBool << endl
             << " Expected Size:    5"        << endl
             << "  Actual Size:     "         << resultSize << endl
             << " GetFront():       " << *testList.GetFront() << endl
             << " GetBack():        " << *testList.GetBack() << endl;

        cout << " GetFront() + PopFront(): ";
        for (int i = 0; i < 5; i++ )
        {
            cout << *testList.GetFront();
            testList.PopFront();
            if ( i < 5-1 ) cout << ", ";
        }
        cout << endl;


        if ( (resultBool) && ( resultSize == 5 ) )  cout << " Pass" << endl;
        else                                         cout << " Fail" << endl;

    }   // Test #7 end

}

void Tester::Test_PopFront()
{
    DrawLine();
    cout << "TEST: Test_PopFront" << endl;
    {   // Test #1: Create an empty int List, PopFront()
        cout << endl << "Test #1: Create an empty int List, PopFront()" << endl;
        List<int> testList;

        bool resultExpected = false;
        bool resultActual   = testList.PopFront();

        cout << " Expected Result: 0 (false)" << endl;
        cout << " Actual Result:   " << resultActual << endl;

        if ( resultExpected == resultActual )   cout << " Pass" << endl;
        else                                    cout << " Fail" << endl;

    }   // Test #1 end

    {   // Test #2: Create an int List, PushBack() 10, PopFront() 10 checking each
        cout << endl << "Test #2: Create an int List, PushBack() 10, PopFront() 10 checking each" << endl;
        List<int> testList;

        for ( int i = 0; i < 10; i++ )
            testList.PushBack(i+101);

        /* ------------------- Testing Purposes Only -------------------
        cout << " LIST: (size: " << testList.Size() << ")" << endl;
        for ( int i = 0; i < 10; i++ )
        {
            cout << "  Get(" << i << ") returns: " << testList.Get(i)
                 << " | dereferenced: " << *testList.Get(i)
                 << " | Size(): " << testList.Size() << endl;
        }
        ---------------------------------------------------------------- */

        bool   popTester;
        string passFail;

        cout << " Check 10 PopFront():" << endl;
        for ( int i = 0; i < 10; i++ )
        {
            cout << "  Size(): " << testList.Size() << "\t| GetFront(): " << *testList.GetFront();
            popTester = testList.PopFront();
            cout << " | PopFront(): " << popTester << " (1 == true, expected)" << endl;
        }
        if   ( popTester ) passFail = "Pass"; // true return expected
        else               passFail = "Fail";
        
        cout << "  Size(): " << testList.Size() << "\t|  (empty list)  ";
        popTester = testList.PopFront();
        cout << " | PopFront(): " << popTester << " (0 == false, expected)" << endl;
        if   ( !popTester ) passFail = "Pass"; // false return expected
        else                passFail = "Fail";
        
        cout << " " << passFail << endl;
 
    }   // Test #2 end

    {   // Test #3: Create an int List, PushBack() 100, PopFront() 100 checking each
        cout << endl << "Test #3: Create int List, PushBack() 100, PopFront() 100 checking each" << endl;
        List<int> testList;

        for ( int i = 0; i < 100; i++ )
            testList.PushBack(i+201);

        /* ------------------- Testing Purposes Only -------------------
        cout << " LIST: (size: " << testList.Size() << ")" << endl;
        for ( int i = 0; i < 10; i++ )
        {
            cout << "  Get(" << i << ") returns: " << testList.Get(i)
                 << " | dereferenced: " << *testList.Get(i)
                 << " | Size(): " << testList.Size() << endl;
        }
        ---------------------------------------------------------------- */

        bool   popTester;
        string passFail;

        cout << " Check 100 PopFront(), print to screen every 10th iteration:" << endl;
        for ( int i = 0; i < 100; i++ )
        {
            if ( (i == 0) || (i % 10 == 0) )
            {
                cout << "  Size(): " << testList.Size() << "\t| GetFront(): " << *testList.GetFront();
                popTester = testList.PopFront();
                cout << " | PopFront(): " << popTester << " (1 == true, expected)" << endl;
            }
            else
            {
                popTester = testList.PopFront();
            }
        }
        if   ( popTester ) passFail = "Pass"; // true return expected
        else               passFail = "Fail";
        
        cout << "  Size(): " << testList.Size() << "\t|  (empty list)  ";
        popTester = testList.PopFront();
        cout << " | PopFront(): " << popTester << " (0 == false, expected)" << endl;
        if   ( !popTester ) passFail = "Pass"; // false return expected
        else                passFail = "Fail";
        
        cout << " " << passFail << endl;
 
    }   // Test #3 end

}

void Tester::Test_PopBack()
{
    DrawLine();
    cout << "TEST: Test_PopBack" << endl;
    {   // Test #1: Create an empty int List, PopBack()
        cout << endl << "Test #1: Create an empty int List, PopBack()" << endl;
        List<int> testList;

        bool resultExpected = false;
        bool resultActual   = testList.PopBack();

        cout << " Expected Result: 0 (false)" << endl;
        cout << " Actual Result:   " << resultActual << endl;

        if ( resultExpected == resultActual )   cout << " Pass" << endl;
        else                                    cout << " Fail" << endl;

    }   // Test #1 end

    {   // Test #2: Create an int List, PushBack() 10, PopBack() 10 checking each
        cout << endl << "Test #2: Create an int List, PushBack() 10, PopBack() 10 checking each" << endl;
        List<int> testList;

        for ( int i = 0; i < 10; i++ )
            testList.PushBack(i+101);

        /* ------------------- Testing Purposes Only -------------------
        cout << " LIST: (size: " << testList.Size() << ")" << endl;
        for ( int i = 0; i < 10; i++ )
        {
            cout << "  Get(" << i << ") returns: " << testList.Get(i)
                 << " | dereferenced: " << *testList.Get(i)
                 << " | Size(): " << testList.Size() << endl;
        }
        ---------------------------------------------------------------- */

        bool   popTester;
        string passFail;

        cout << " Check 10 PopBack():" << endl;
        for ( int i = 0; i < 10; i++ )
        {
            cout << "  Size(): " << testList.Size() << "\t| GetBack(): " << *testList.GetBack();
            popTester = testList.PopBack();
            cout << " | PopBack(): " << popTester << " (1 == true, expected)" << endl;
        }
        if   ( popTester ) passFail = "Pass"; // true return expected
        else               passFail = "Fail";
        
        cout << "  Size(): " << testList.Size() << "\t|  (empty list) ";
        popTester = testList.PopBack();
        cout << " | PopBack(): " << popTester << " (0 == false, expected)" << endl;
        if   ( !popTester ) passFail = "Pass"; // false return expected
        else                passFail = "Fail";
        
        cout << " " << passFail << endl;
 
    }   // Test #2 end

    {   // Test #3: Create an int List, PushBack() 100, PopBack() 100 checking each
        cout << endl << "Test #3: Create int List, PushBack() 100, PopBack() 100 checking each" << endl;
        List<int> testList;

        for ( int i = 0; i < 100; i++ )
            testList.PushBack(i+201);

        /* ------------------- Testing Purposes Only -------------------
        cout << " LIST: (size: " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  Get(" << i << ") returns: " << testList.Get(i)
                 << " | dereferenced: " << *testList.Get(i)
                 << " | Size(): " << testList.Size() << endl;
        }
        ---------------------------------------------------------------- */

        bool   popTester;
        string passFail;

        cout << " Check 100 PopBack(), print to screen every 10th iteration:" << endl;
        for ( int i = 0; i < 100; i++ )
        {
            if ( (i == 0) || (i % 10 == 0) )
            {
                cout << "  Size(): " << testList.Size() << "\t| GetBack(): " << *testList.GetBack();
                popTester = testList.PopBack();
                cout << " | PopBack(): " << popTester << " (1 == true, expected)" << endl;
            }
            else
            {
                popTester = testList.PopBack();
            }
        }
        if   ( popTester ) passFail = "Pass"; // true return expected
        else               passFail = "Fail";
        
        cout << "  Size(): " << testList.Size() << "\t|  (empty list) ";
        popTester = testList.PopBack();
        cout << " | PopBack(): " << popTester << " (0 == false, expected)" << endl;
        if   ( !popTester ) passFail = "Pass"; // false return expected
        else                passFail = "Fail";
        
        cout << " " << passFail << endl;
 
    }   // Test #3 end

}

void Tester::Test_Clear()
{
    DrawLine();
    cout << "TEST: Test_Clear" << endl;
    {   // Test #1: Create a List, add 10 items, Clear() it
        cout << endl << "Test #1: Create a List, add 10 items, Clear() it" << endl;
        List<int> testList;
        for ( int i = 0; i < 10; i++ )
            testList.PushFront(i+101);

        cout << " List before Clear(): size   " << testList.Size() << endl;

        /* ------------------- Testing Purposes Only -------------------
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  Get(" << i << ") returns: " << testList.Get(i)
                 << " | dereferenced: " << *testList.Get(i)
                 << " | Size(): " << testList.Size() << endl;
        }
        ---------------------------------------------------------------- */

        testList.Clear();
        cout << " List after  Clear(): size    " << testList.Size() << endl;

    }   // Test #1 end

    {   // Test #2: Create a List, add 100 items, Clear() it
        cout << endl << "Test #2: Create a List, add 100 items, Clear() it" << endl;
        List<int> testList;
        for ( int i = 0; i < 100; i++ )
            testList.PushFront(i+201);

        cout << " List before Clear(): size  " << testList.Size() << endl;

        /* ------------------- Testing Purposes Only -------------------
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  Get(" << i << ") returns: " << testList.Get(i)
                 << " | dereferenced: " << *testList.Get(i)
                 << " | Size(): " << testList.Size() << endl;
        }
        ---------------------------------------------------------------- */

        testList.Clear();
        cout << " List after  Clear(): size    " << testList.Size() << endl;

    }   // Test #2 end

    {   // Test #3: Create an empty List, Clear() it
        cout << endl << "Test #3: Create an empty List, Clear() it" << endl;
        List<int> testList;
        cout << " List before Clear(): size    " << testList.Size() << endl;
        testList.Clear();
        cout << " List after  Clear(): size    " << testList.Size() << endl;
        
        
    }   // Test #3 end

}

void Tester::Test_Get() 
{
    DrawLine();
    cout << "TEST: Test_Get" << endl;

    {   // Test #1: Create an int List, PushBack() 10 items, return index 9
        cout << endl << "Test #1: Create an int List, PushBack() 10 items, return index 9" << endl;
        List<int> testList;

        for ( int i = 0; i < 10; i++ )
        {
            testList.PushBack(i+101);
        }

        int  resultExpected = 110;
        int* resultActual   = testList.Get(9);

        cout << " Expected Result: " << resultExpected << endl
             << " Actual Result:   " << *resultActual  << endl;

        /* ------------------- Testing Purposes Only -------------------
        for ( int i = 0; i < 10; i++ )
        {
            cout << " Get(" << i << ") returns: " << testList.Get(i)
                 << " | dereferenced: " << *testList.Get(i) << endl;
        }
        ---------------------------------------------------------------- */

        if ( resultExpected == *resultActual ) cout << " Pass" << endl;
        else                                   cout << " Fail" << endl;

    }   // Test #1 end

    {   // Test #2: Create an int List, PushBack() 10 items, return index 10
        cout << endl << "Test #2: Create an int List, PushBack() 10 items, return index 10" << endl;
        List<int> testList;

        for ( int i = 0; i < 10; i++ )
        {
            testList.PushBack(i+101);
        }

        int* resultExpected = nullptr;
        int* resultActual   = testList.Get(10);

        cout << " Expected Result: " << resultExpected << " (nullptr == 0)" << endl
             << " Actual Result:   " << resultActual   << " (nullptr == 0)" << endl;

        /* ------------------- Testing Purposes Only -------------------
        for ( int i = 0; i < 10; i++ )
        {
            cout << " Get(" << i << ") returns: " << testList.Get(i)
                 << " | dereferenced: " << *testList.Get(i) << endl;
        }
        ---------------------------------------------------------------- */

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;

    }   // Test #2 end

    {   // Test #3: Create a string List, PushBack() 10 items, return index 9
        cout << endl << "Test #3: Create a string List, PushBack() 10 items, return index 9" << endl;
        List<string> testList;

        string strVar;
        for ( int i = 0; i < 10; i++ )
        {
            strVar = "string0" + to_string(i);
            testList.PushBack(strVar);
        }

        string resultExpected = "string09";
        string resultActual   = *testList.Get(9);

        cout << " Expected Result: " << resultExpected << endl
             << " Actual Result:   " << resultActual   << endl;

        /* ------------------- Testing Purposes Only -------------------
        for ( int i = 0; i < 10; i++ )
        {
            cout << " Get(" << i << ") returns: " << testList.Get(i)
                 << " | dereferenced: " << *testList.Get(i) << endl;
        }
        ---------------------------------------------------------------- */

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;

    }   // Test #3 end

    {   // Test #4: Create a string List, PushBack() 10 items, return index 10
        cout << endl << "Test #4: Create a string List, PushBack() 10 items, return index 10" << endl;
        List<string> testList;

        string strVar;
        for ( int i = 0; i < 10; i++ )
        {
            strVar = "string0" + to_string(i);
            testList.PushBack(strVar);
        }

        string* resultExpected = nullptr;
        string* resultActual   = testList.Get(10);

        cout << " Expected Result: " << resultExpected << " (nullptr == 0)" << endl
             << " Actual Result:   " << resultActual   << " (nullptr == 0)" << endl;

        /* ------------------- Testing Purposes Only -------------------
        for ( int i = 0; i < 10; i++ )
        {
            cout << " Get(" << i << ") returns: " << testList.Get(i)
                 << " | dereferenced: " << *testList.Get(i) << endl;
        }
        ---------------------------------------------------------------- */

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;

    }   // Test #4 end
}

void Tester::Test_GetFront()
{
    DrawLine();
    cout << "TEST: Test_GetFront" << endl;

    {   // Test #1: Create an int List, PushBack() 10 items, GetFront()
        cout << endl << "Test #1: Create an int List, PushBack() 10 items, GetFront()" << endl;
        List<int> testList;

        for ( int i = 0; i < 10; i++ )
            testList.PushBack(i+101);

        int resultExpected = 101;
        int resultActual   = *testList.GetFront();

        cout << " Expected Result: " << resultExpected << endl
             << " Actual Result:   " << resultActual   << endl;

        cout << " GetFront(): " << testList.GetFront() 
             << " | dereferenced: " << *testList.GetFront() << endl;

        cout << " Get(0):     " << testList.Get(0)
             << " | dereferenced: " << *testList.Get(0) << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;

    }   // Test #1 end

    {   // Test #2: Create an int List, PushFront() 10 items, GetFront()
        cout << endl << "Test #2: Create an int List, PushFront() 10 items, GetFront()" << endl;
        List<int> testList;

        for ( int i = 0; i < 10; i++ )  testList.PushFront(i+201);
        for ( int i = 0; i < 5; i++ )   testList.PopFront();

        int resultExpected = 205;
        int resultActual   = *testList.GetFront();

        cout << " Expected Result: " << resultExpected << endl
             << " Actual Result:   " << resultActual   << endl;

        cout << " GetFront(): " << testList.GetFront() 
             << " | dereferenced: " << *testList.GetFront() << endl;

        cout << " Get(0):     " << testList.Get(0)
             << " | dereferenced: " << *testList.Get(0) << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;

    }   // Test #2 end

    {   // Test #3: Create an int List, leave it empty, GetFront()
        cout << endl << "Test #3: Create an int List, leave it empty, GetFront()" << endl;
        List<int> testList;

        int* resultExpected = nullptr;
        int* resultActual   = testList.GetFront();

        cout << " Expected Result: " << resultExpected << " (nullptr == 0)" << endl
             << " Actual Result:   " << resultActual   << " (nullptr == 0)" << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;

    }   // Test #3 end

    {   // Test #4: Create a string List, PushBack() 10 items, PopFront() 2, GetFront()
        cout << endl << "Test #4: Create a string List, PushBack() 10, PopFront() 2, GetFront()" << endl;
        List<string> testList;

        string strVar;
        for ( int i = 0; i < 10; i++ )
        {
            strVar = "string0" + to_string(i);
            testList.PushBack(strVar);
        }
        
        /* ------------------- Testing Purposes Only -------------------
        cout << " LIST:" << endl;
        for ( int i = 0; i < 10; i++ )
        {
            cout << "  Get(" << i << ") returns: " << testList.Get(i)
                 << " | dereferenced: " << *testList.Get(i) << endl;
        }
        ---------------------------------------------------------------- */

        testList.PopFront();
        testList.PopFront();

        string resultExpected = "string02";
        string resultActual   = *testList.GetFront();

        cout << " Expected Result: " << resultExpected << endl
             << " Actual Result:   " << resultActual   << endl;

        cout << " GetFront(): " << testList.GetFront() 
             << " | dereferenced: " << *testList.GetFront() << endl;

        cout << " Get(0):     " << testList.Get(0)
             << " | dereferenced: " << *testList.Get(0) << endl;

        /* ------------------- Testing Purposes Only -------------------
        cout << " LIST: " << endl;
        for ( int i = 0; i < 8; i++ )
        {
            cout << "  Get(" << i << ") returns: " << testList.Get(i)
                 << " | dereferenced: " << *testList.Get(i) << endl;
        }
        ---------------------------------------------------------------- */

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;

    }   // Test #4 end
}

void Tester::Test_GetBack()
{
    DrawLine();
    cout << "TEST: Test_GetBack" << endl;

    {   // Test #1: Create an int List, PushBack() 10 items, GetBack()
        cout << "Test #1: Create an int List, PushBack() 10 items, GetBack()" << endl;
        List<int> testList;

        for ( int i = 0; i < 10; i++ )
            testList.PushBack(i+101);

        int resultExpected = 110;
        int resultActual   = *testList.GetBack();

        cout << " Expected Result: " << resultExpected << endl
             << " Actual Result:   " << resultActual   << endl;

        cout << " GetBack(): " << testList.GetBack() 
             << " | dereferenced: " << *testList.GetBack() << endl;

        cout << " Get(9):    " << testList.Get(9)
             << " | dereferenced: " << *testList.Get(9) << endl;

        /* ------------------- Testing Purposes Only -------------------
        cout << " LIST: " << endl;
        for ( int i = 0; i < 10; i++ )
        {
            cout << "  Get(" << i << ") returns: " << testList.Get(i)
                 << " | dereferenced: " << *testList.Get(i) << endl;
        }
        ---------------------------------------------------------------- */

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;

    }   // Test #1 end

    {   // Test #2: Create an int List, PushFront() 10 , PopFront() 5, GetBack()
        cout << endl << "Test #2: Create an int List, PushFront() 10, "
                     << "PopFront() 5, GetBack()" << endl;
        List<int> testList;

        for ( int i = 0; i < 10; i++ )
            testList.PushFront(i+201);

        /* ------------------- Testing Purposes Only -------------------
        cout << " LIST: " << endl;
        for ( int i = 0; i < 10; i++ )
        {
            cout << "  Get(" << i << ") returns: " << testList.Get(i)
                 << " | dereferenced: " << *testList.Get(i) << endl;
        }
        ---------------------------------------------------------------- */

        for ( int i = 0; i < 5; i++ )
            testList.PopFront();

        int resultExpected = 201;
        int resultActual   = *testList.GetBack();

        cout << " Expected Result: " << resultExpected << endl
             << " Actual Result:   " << resultActual   << endl;

        cout << " GetBack(): " << testList.GetBack() 
             << " | dereferenced: " << *testList.GetBack() << endl;

        cout << " Get(4):    " << testList.Get(4)
             << " | dereferenced: " << *testList.Get(4) << endl;

        /* ------------------- Testing Purposes Only -------------------
        cout << " LIST: " << endl;
        for ( int i = 0; i < 5; i++ )
        {
            cout << "  Get(" << i << ") returns: " << testList.Get(i)
                 << " | dereferenced: " << *testList.Get(i) << endl;
        }
        ---------------------------------------------------------------- */

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;

    }   // Test #2 end

    {   // Test #3: Create an int List, leave it empty, GetBack()
        cout << endl << "Test #3: Create an int List, leave it empty, GetBack()" << endl;
        List<int> testList;

        int* resultExpected = nullptr;
        int* resultActual   = testList.GetBack();

        cout << " Expected Result: " << resultExpected << " (nullptr == 0)" << endl
             << " Actual Result:   " << resultActual   << " (nullptr == 0)" << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;

    }   // Test #3 end

    {   // Test #4: Create a string List, PushBack() 10 items, PopFront() 2, GetBack()
        cout << endl << "Test #4: Create a string List, PushBack() 10, PopFront() 2, GetBack()" << endl;
        List<string> testList;

        string strVar;
        for ( int i = 0; i < 10; i++ )
        {
            strVar = "string0" + to_string(i);
            testList.PushBack(strVar);
        }
        
        /* ------------------- Testing Purposes Only -------------------
        cout << " LIST:" << endl;
        for ( int i = 0; i < 10; i++ )
        {
            cout << "  Get(" << i << ") returns: " << testList.Get(i)
                 << " | dereferenced: " << *testList.Get(i) << endl;
        }
        ---------------------------------------------------------------- */

        testList.PopFront();
        testList.PopFront();

        string resultExpected = "string09";
        string resultActual   = *testList.GetBack();

        cout << " Expected Result: " << resultExpected << endl
             << " Actual Result:   " << resultActual   << endl;

        cout << " GetBack(): " << testList.GetBack() 
             << " | dereferenced: " << *testList.GetBack() << endl;

        cout << " Get(7):    " << testList.Get(7)
             << " | dereferenced: " << *testList.Get(7) << endl;

        /* ------------------- Testing Purposes Only -------------------
        cout << " LIST: " << endl;
        for ( int i = 0; i < 8; i++ )
        {
            cout << "  Get(" << i << ") returns: " << testList.Get(i)
                 << " | dereferenced: " << *testList.Get(i) << endl;
        }
        ---------------------------------------------------------------- */

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;
    }

}

void Tester::Test_GetCountOf()
{
    DrawLine();
    cout << "TEST: Test_GetCountOf" << endl;
    {   // Test #1: Test strings, input included 1 out of 10
        cout << endl << "Test #1: Test strings, input included 1 out of 10" << endl;
        List<string> testList;

        for ( int i = 0; i < 10; i++ )
        {
            if ( (i+1) % 10 == 0 )  testList.PushBack("findme");
            else                    testList.PushBack("algo");
        }

        /* ------------------- Testing Purposes Only -------------------
        ---------------------------------------------------------------- */
        cout << " Size: " << testList.Size() << endl
             << " LIST: " << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  testList[" << i << "] => " << *testList.Get(i) << "\t";
            if ( (i+1) % 3 == 0) cout << endl;
        }
        cout << endl;

        int expectedResult(1);
        int actualResult = testList.GetCountOf("findme");

        cout << " Expected Result: " << expectedResult << endl;
        cout << " Actual Result:   " << actualResult   << endl;

        if ( expectedResult == actualResult )   cout << " Pass" << endl;
        else                                    cout << " Fail" << endl;

    }   // Test #1 end

    {   // Test #2: Test strings, input included 5 out of 50
        cout << endl << "Test #2: Test strings, input included 5 out of 50" << endl;
        List<string> testList;

        for ( int i = 0; i < 50; i++ )
        {
            if ( (i+1) % 10 == 0 )  testList.PushBack("findme");
            else                    testList.PushBack("whatever");
        }

        int expectedResult(5);
        int actualResult = testList.GetCountOf("findme");

        cout << " Expected Result: " << expectedResult << endl;
        cout << " Actual Result:   " << actualResult   << endl;

        if ( expectedResult == actualResult )   cout << " Pass" << endl;
        else                                    cout << " Fail" << endl;
    }   // Test #2 end

    {   // Test #3: Test strings, input included 10 out of 100
        cout << endl << "Test #3: Test strings, input included 10 out of 100" << endl;
        List<string> testList;

        for ( int i = 0; i < 100; i++ )
        {
            if ( (i+1) % 10 == 0 )  testList.PushBack("findme");
            else                    testList.PushBack("whatever");
        }

        int expectedResult(10);
        int actualResult = testList.GetCountOf("findme");

        cout << " Expected Result: " << expectedResult << endl;
        cout << " Actual Result:   " << actualResult   << endl;

        if ( expectedResult == actualResult )   cout << " Pass" << endl;
        else                                    cout << " Fail" << endl;
    }   // Test #3 end

    {   // Test #4: Test strings, input included 0 out of 100
        cout << endl << "Test #4: Test strings, input included 0 out of 100" << endl;
        List<string> testList;

        for ( int i = 0; i < 100; i++ ) 
            testList.PushBack("whatever");

        int expectedResult(0);
        int actualResult = testList.GetCountOf("findme");

        cout << " Expected Result: " << expectedResult << endl;
        cout << " Actual Result:   " << actualResult   << endl;

        if ( expectedResult == actualResult )   cout << " Pass" << endl;
        else                                    cout << " Fail" << endl;
    }   // Test #4 end

    {   // Test #5: Test integers, input included 1 out of 10
        cout << endl << "Test #5: Test integers, input included 1 out of 10" << endl;
        List<int> testList;

        for ( int i = 0; i < 10; i++ )
        {
            if ( (i+1) % 10 == 0 )  testList.PushBack(777);
            else                    testList.PushBack(i+111);
        }

        /* ------------------- Testing Purposes Only -------------------
        ---------------------------------------------------------------- */
        cout << " Size: " << testList.Size() << endl
             << " LIST: " << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  testList[" << i << "] => " << *testList.Get(i) << "\t";
            if ( (i+1) % 3 == 0) cout << endl;
        }
        cout << endl;

        int expectedResult(1);
        int actualResult = testList.GetCountOf(777);

        cout << " Expected Result: " << expectedResult << endl;
        cout << " Actual Result:   " << actualResult   << endl;

        if ( expectedResult == actualResult ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;

    }   // Test #5 end

    {   // Test #6: Test integers, input included 5 out of 50
        cout << endl << "Test #6: Test integers, input included 5 out of 50" << endl;
        List<int> testList;

        for ( int i = 0; i < 50; i++ )
        {
            if ( (i+1) % 10 == 0 )  testList.PushBack(777);
            else                    testList.PushBack(i+1);
        }

        int expectedResult(5);
        int actualResult = testList.GetCountOf(777);

        cout << " Expected Result: " << expectedResult << endl;
        cout << " Actual Result:   " << actualResult   << endl;

        if ( expectedResult == actualResult )   cout << " Pass" << endl;
        else                                    cout << " Fail" << endl;

    }   // Test #6 end

    {   // Test #7: Test integers, input included 10 out of 100
        cout << endl << "Test #7: Test integers, input included 10 out of 100" << endl;
        List<int> testList;

        for ( int i = 0; i < 100; i++ )
        {
            if ( (i+1) % 10 == 0 )  testList.PushBack(777);
            else                    testList.PushBack(i+1);
        }

        int expectedResult(10);
        int actualResult = testList.GetCountOf(777);

        cout << " Expected Result: " << expectedResult << endl;
        cout << " Actual Result:   " << actualResult   << endl;

        if ( expectedResult == actualResult )   cout << " Pass" << endl;
        else                                    cout << " Fail" << endl;

    }   // Test #7 end

    {   // Test #8: Test integers, input included 0 out of 100
        cout << endl << "Test #8: Test integers, input included 0 out of 100" << endl;
        List<int> testList;

        for ( int i = 0; i < 100; i++ )
            testList.PushBack(i+1);

        int expectedResult(0);
        int actualResult = testList.GetCountOf(777);

        cout << " Expected Result: " << expectedResult << endl;
        cout << " Actual Result:   " << actualResult   << endl;

        if ( expectedResult == actualResult )   cout << " Pass" << endl;
        else                                    cout << " Fail" << endl;
    }   // Test #8 end
}

void Tester::Test_Contains()  // ============== WRITE FUNCTION IN LIST.HPP ========================== OJO <==
{
    DrawLine();
    cout << "TEST: Test_Contains" << endl;

    {   // Test #1: Test strings, input included
        cout << endl << "Test 1: Test strings, input included" << endl;
        List<string> testList;
        
        for ( int i = 0; i < 10; i++ )
        {
            if ( (i+1) % 10 == 0 )  testList.PushBack("findme");
            else                    testList.PushBack("whatever");
        }
    
        cout << " Expected Result: 1 (true)" << endl;
        cout << " Actual Result:   " << testList.Contains("findme") << endl;

        if ( testList.Contains("findme") )  cout << " Pass" << endl;
        else                                cout << " Fail" << endl;
    }   // Test #1 end

    {   // Test #2: Test strings, input excluded
        cout << endl << "Test #2: Test strings, input excluded" << endl;
        List<string> testList;

        for ( int i = 0; i < 100; i++ ) 
            testList.PushBack("whatever");

        cout << " Expected Result: 0 (false)" << endl;
        cout << " Actual Result:   " << testList.Contains("findme") << endl;

        if ( !testList.Contains("findme") )  cout << " Pass" << endl;
        else                                 cout << " Fail" << endl;
    }   // Test #2 end 

    {   // Test #3: Test integers, input included
        cout << endl << "Test #3: Test integers, input included" << endl;
        List<int> testList;

        for ( int i = 0; i < 10; i++ )
        {
            if ( (i+1) % 10 == 0 )  testList.PushBack(777);
            else                    testList.PushBack(i+1);
        }
 
        cout << " Expected Result: 1 (true)" << endl;
        cout << " Actual Result:   " << testList.Contains(777) << endl;

        if ( testList.Contains(777) )   cout << " Pass" << endl;
        else                            cout << " Fail" << endl;
    }   // Test #3 end

    {   // Test #4: Test integers, input excluded
        cout << endl << "Test #4: Test integers, input excluded" << endl;
        List<int> testList;
        
        for ( int i = 0; i < 100; i++ )
            testList.PushBack(i+1);

        cout << " Expected Result: 0 (false)" << endl;
        cout << " Actual Result:   " << testList.Contains(777) << endl;

        if ( !testList.Contains(777) )  cout << " Pass" << endl;
        else                            cout << " Fail" << endl;
    }   // Test #4 end
}

void Tester::Test_RemoveItem()
{
    DrawLine();
    cout << "TEST: Test_RemoveItem()" << endl;

    {   // Test #1: Create an int List, PushBack() 10 items, 777 four times
        cout << endl << "Test #1: Create an int List, PushBack() 10 items, 777 four times" << endl;
        List<int> testList;
    
        for ( int i = 0; i < 10; i++ )
        {
            if ( i % 3 == 0 )   testList.PushBack(777);
            else                testList.PushBack(i+1);
        }

        cout << " ORIGINAL LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  testList[" << i << "] => " << *testList.Get(i) << "\t";
            if ( (i+1) % 3 == 0) cout << endl;
        }
        
        cout << endl << " RemoveItem(777)" << endl;
        bool resultExpected = true;
        bool resultActual   = testList.RemoveItem(777);

        cout << " MODIFIED LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  testList[" << i << "] => " << *testList.Get(i) << "\t";
            if ( (i+1) % 3 == 0) cout << endl;
        }
        
        cout << " Expected Result: " << resultExpected << " (1 == true)" << endl;
        cout << " Actual Result:   " << resultActual << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;

    }   // Test #1 end

    {   // Test #2: Create an int List, PushBack() 10 items, 777 zero times
        cout << endl << "Test #2: Create an int List, PushBack() 10 items, 777 zero times" << endl;
        List<int> testList;
    
        for ( int i = 0; i < 10; i++ )
            testList.PushBack(i+1);

        cout << " ORIGINAL LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  testList[" << i << "] => " << *testList.Get(i) << "\t";
            if ( (i+1) % 3 == 0) cout << endl;
        }
        
        cout << endl << " RemoveItem(777)" << endl;
        bool resultExpected = false;
        bool resultActual   = testList.RemoveItem(777);

        cout << " MODIFIED LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  testList[" << i << "] => " << *testList.Get(i) << "\t";
            if ( (i+1) % 3 == 0) cout << endl;
        }
        
        cout << endl;
        cout << " Expected Result: " << resultExpected << " (0 == false)" << endl;
        cout << " Actual Result:   " << resultActual << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;

    }   // Test #2 end

    {   // Test #3: Create a string List, PushBack() 10 item, "findme" four times
        cout << endl << "Test #3: Create a string List, PushBack() 10 item, \"findme\" four times" << endl;
        List<string> testList;

        for ( int i = 0; i < 10; i++ )
        {
            if ( i % 3 == 0 )   testList.PushBack("findme");
            else                testList.PushBack("algo");
        }

        cout << " ORIGINAL LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  testList[" << i << "] => " << *testList.Get(i) << "\t";
            if ( (i+1) % 3 == 0) cout << endl;
        }
        
        cout << endl << " RemoveItem(\"findme\")" << endl;
        bool resultExpected = true;
        bool resultActual   = testList.RemoveItem("findme");

        cout << " MODIFIED LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  testList[" << i << "] => " << *testList.Get(i) << "\t";
            if ( (i+1) % 3 == 0) cout << endl;
        }
        
        cout << " Expected Result: " << resultExpected << " (1 == true)" << endl;
        cout << " Actual Result:   " << resultActual << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;
    }   // Test #3 end

    {   // Test #4: Create a string List, PushBack() 10 item, "findme" zero times
        cout << endl << "Test #4: Create a string List, PushBack() 10 item, \"findme\" zero times" << endl;
        List<string> testList;

        for ( int i = 0; i < 10; i++ )
            testList.PushBack("algo");

        cout << " ORIGINAL LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  testList[" << i << "] => " << *testList.Get(i) << "\t";
            if ( (i+1) % 3 == 0) cout << endl;
        }
        
        cout << endl << " RemoveItem(\"findme\")" << endl;
        bool resultExpected = false;
        bool resultActual   = testList.RemoveItem("findme");

        cout << " MODIFIED LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  testList[" << i << "] => " << *testList.Get(i) << "\t";
            if ( (i+1) % 3 == 0) cout << endl;
        }
        
        cout << endl;
        cout << " Expected Result: " << resultExpected << " (0 == false)" << endl;
        cout << " Actual Result:   " << resultActual << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;
    }   // Test #4 end

    {   // Test #5: Create and empty List
        cout << endl << "Test #5: Create an empty List" << endl;
        List<int> testList;

        cout << " ORIGINAL LIST: (size " << testList.Size() << ")" << endl;

        cout << " RemoveItem(777)" << endl;
        bool resultExpected = false;
        bool resultActual   = testList.RemoveItem(777);

        cout << " MODIFIED LIST: (size " << testList.Size() << ")" << endl;
        
        cout << " Expected Result: " << resultExpected << " (0 == false)" << endl;
        cout << " Actual Result:   " << resultActual << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;
    }   // Test #4 end
}

void Tester::Test_RemoveIndex()
{
    DrawLine();
    cout << "TEST: Test_RemoveIndex()" << endl;

    {   // Test #1: Create an int List, leave it empty, Remove() 1 item
        cout << endl << "Test #1: Create an int List, leave it empty, Remove() 1 item" << endl;
        List<int> testList;
        
        cout << " Size before Remove(): " << testList.Size() << endl;
        bool resultExpected = false;
        bool resultActual   = testList.RemoveIndex(1);
        cout << " Size after  Remove(): " << testList.Size() << endl;

        cout << " Expected Result: " << resultExpected << " (0 == false)" << endl;
        cout << " Actual Result:   " << resultActual << endl;

        if ( resultExpected == resultActual )    cout << " Pass" << endl;
        else                                     cout << " Fail" << endl;

    }   // Test #1 end

    {   // Test #2: Create an int List, PushBack() 10 items, Remove() 1 item
        cout << endl << "Test #2: Create an int List, PushBack() 10 items, Remove() 1 item" << endl;
        List<int> testList;
    
        for ( int i = 0; i < 10; i++ )
            testList.PushBack(i+1);

        cout << " ORIGINAL LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  testList[" << i << "] => " << *testList.Get(i) << "\t";
            if ( (i+1) % 3 == 0) cout << endl;
        }
        
        cout << endl << " RemoveIndex(3): 4" << endl;
        bool resultExpected = true;
        bool resultActual   = testList.RemoveIndex(3);

        cout << " MODIFIED LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  testList[" << i << "] => " << *testList.Get(i) << "\t";
            if ( (i+1) % 3 == 0) cout << endl;
        }
        
        cout << " Expected Result: " << resultExpected << " (1 == true)" << endl;
        cout << " Actual Result:   " << resultActual << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;

    }   // Test #2 end

    {   // Test #3: Create an int List, PushBack() 10 items, Remove() first item
        cout << endl << "Test #3: Create an int List, PushBack() 10 items, Remove() first item" << endl;
        List<int> testList;
    
        for ( int i = 0; i < 10; i++ )
            testList.PushBack(i+1);

        cout << " ORIGINAL LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  testList[" << i << "] => " << *testList.Get(i) << "\t";
            if ( (i+1) % 3 == 0) cout << endl;
        }
        
        cout << endl << " RemoveIndex(0): 1" << endl;
        bool resultExpected = true;
        bool resultActual   = testList.RemoveIndex(0);

        cout << " MODIFIED LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  testList[" << i << "] => " << *testList.Get(i) << "\t";
            if ( (i+1) % 3 == 0) cout << endl;
        }
        
        cout << " Expected Result: " << resultExpected << " (1 == true)" << endl;
        cout << " Actual Result:   " << resultActual << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;

    }   // Test #3 end

    {   // Test #4: Create an int List, PushBack() 10 items, Remove() last item
        cout << endl << "Test #4: Create an int List, PushBack() 10 items, Remove() last item" << endl;
        List<int> testList;
    
        for ( int i = 0; i < 10; i++ )
            testList.PushBack(i+1);

        cout << " ORIGINAL LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  testList[" << i << "] => " << *testList.Get(i) << "\t";
            if ( (i+1) % 3 == 0) cout << endl;
        }
        
        cout << endl << " RemoveIndex(9): 10" << endl;
        bool resultExpected = true;
        bool resultActual   = testList.RemoveIndex(9);

        cout << " MODIFIED LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  testList[" << i << "] => " << *testList.Get(i) << "\t";
            if ( (i+1) % 3 == 0) cout << endl;
        }
        
        cout << " Expected Result: " << resultExpected << " (1 == true)" << endl;
        cout << " Actual Result:   " << resultActual << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;

    }   // Test #4 end

    {   // Test #5: Create an int List, PushBack() 10 items, Remove() 1 item, negative index
        cout << endl << "Test #5: Create int List, PushBack() 10, Remove() with negative index" << endl;
        List<int> testList;
    
        for ( int i = 0; i < 10; i++ )
            testList.PushBack(i+1);

        cout << " ORIGINAL LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  testList[" << i << "] => " << *testList.Get(i) << "\t";
            if ( (i+1) % 3 == 0) cout << endl;
        }
        
        cout << endl << " RemoveIndex(-1): out-of-bounds" << endl;
        bool resultExpected = false;
        bool resultActual   = testList.RemoveIndex(-1);

        cout << " MODIFIED LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  testList[" << i << "] => " << *testList.Get(i) << "\t";
            if ( (i+1) % 3 == 0) cout << endl;
        }
        
        cout << endl;
        cout << " Expected Result: " << resultExpected << " (0 == false)" << endl;
        cout << " Actual Result:   " << resultActual << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;

    }   // Test #5 end

    {   // Test #6: Create int List, PushBack() 10, Remove() 1 with out-of-bounds argument
        cout << endl << "Test #6: Create int List, PushBack() 10, Remove() out-of-bounds arg" << endl;
        List<int> testList;
    
        for ( int i = 0; i < 10; i++ )
            testList.PushBack(i+1);

        cout << " ORIGINAL LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  testList[" << i << "] => " << *testList.Get(i) << "\t";
            if ( (i+1) % 3 == 0) cout << endl;
        }
        
        cout << endl << " RemoveIndex(15): out-of-bounds" << endl;
        bool resultExpected = false;
        bool resultActual   = testList.RemoveIndex(15);

        cout << " MODIFIED LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << "  testList[" << i << "] => " << *testList.Get(i) << "\t";
            if ( (i+1) % 3 == 0) cout << endl;
        }
        
        cout << endl;
        cout << " Expected Result: " << resultExpected << " (0 == false)" << endl;
        cout << " Actual Result:   " << resultActual << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl;
        else                                  cout << " Fail" << endl;

    }   // Test #6 end
}
void Tester::Test_Insert()
{
    DrawLine();
    cout << "TEST: Test_Insert" << endl;
    {   // Test #1: Create a string List with 10 items, Insert() at index 0 (first)
        cout << endl << "Test #1: Create a string List with 10 items, Insert() at index 0" << endl;
        List<string> testList;

        string strVar;
        for ( int i = 0; i < 10; i++ )
        {
            strVar = "string0" + to_string(i);
            testList.PushBack(strVar);
        }

        cout << " ORIGINAL LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << " " << setw(2)  << (i+1) << ". " << *testList.Get(i) << "\t";
            if ( (i+1) % 4 == 0) cout << endl;
        }
        cout << endl << endl;

        bool resultExpected = true;
        bool resultActual   = (testList.Insert(0, "INSERTED") );

        cout << " MODIFIED LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << " " << setw(2)  << (i+1) << ". " << *testList.Get(i) << "\t";
            if ( (i+1) % 4 == 0) cout << endl;
        }

        cout << endl << endl
             << " Expected Result: " << resultExpected << " (1 == true)" << endl
             << " Actual Result:   " << resultActual   << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl << endl;
        else                                  cout << " Fail" << endl << endl;

    }   // Test #1 end

    {   // Test #2 Create a string List with 10 items, Insert() at index 5
        cout << endl << "Test #2: Create a string List with 10 items, Insert() at index 5" << endl;
        List<string> testList;

        string strVar;
        for ( int i = 0; i < 10; i++ )
        {
            strVar = "string0" + to_string(i);
            testList.PushBack(strVar);
        }

        cout << " ORIGINAL LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << " " << setw(2)  << (i+1) << ". " << *testList.Get(i) << "\t";
            if ( (i+1) % 4 == 0) cout << endl;
        }
        cout << endl << endl;

        bool resultExpected = true;
        bool resultActual   = (testList.Insert(5, "INSERTED") );

        cout << " MODIFIED LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << " " << setw(2)  << (i+1) << ". " << *testList.Get(i) << "\t";
            if ( (i+1) % 4 == 0) cout << endl;
        }

        cout << endl << endl
             << " Expected Result: " << resultExpected << " (1 == true)" << endl
             << " Actual Result:   " << resultActual   << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl << endl;
        else                                  cout << " Fail" << endl << endl;

    }   // Test #2 end

    {   // Test #3: Create a string List with 10 items, Insert() at index 9 index
        cout << endl << "Test #3: Create a string List with 10 items, Insert() at index 9" << endl;
        List<string> testList;

        string strVar;
        for ( int i = 0; i < 10; i++ )
        {
            strVar = "string0" + to_string(i);
            testList.PushBack(strVar);
        }

        cout << " ORIGINAL LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << " " << setw(2)  << (i+1) << ". " << *testList.Get(i) << "\t";
            if ( (i+1) % 4 == 0) cout << endl;
        }
        cout << endl << endl;

        bool resultExpected = true;
        bool resultActual   = (testList.Insert(9, "INSERTED") );

        cout << " MODIFIED LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << " " << setw(2)  << (i+1) << ". " << *testList.Get(i) << "\t";
            if ( (i+1) % 4 == 0) cout << endl;
        }

        cout << endl << endl
             << " Expected Result: " << resultExpected << " (1 == true)" << endl
             << " Actual Result:   " << resultActual   << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl << endl;
        else                                  cout << " Fail" << endl << endl;

    }   // Test #3 end

    {   // Test #4: Create a string List with 10 items, Insert() at index 10 (m_itemCount)
        cout << endl << "Test #4: Create a string List with 10 items, Insert() at 10 (m_itemCount)" << endl;
        List<string> testList;

        string strVar;
        for ( int i = 0; i < 10; i++ )
        {
            strVar = "string0" + to_string(i);
            testList.PushBack(strVar);
        }

        cout << " ORIGINAL LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << " " << setw(2)  << (i+1) << ". " << *testList.Get(i) << "\t";
            if ( (i+1) % 4 == 0) cout << endl;
        }
        cout << endl << endl;

        bool resultExpected = true;
        bool resultActual   = (testList.Insert(10, "INSERTED") );

        cout << " MODIFIED LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << " " << setw(2)  << (i+1) << ". " << *testList.Get(i) << "\t";
            if ( (i+1) % 4 == 0) cout << endl;
        }

        cout << endl << endl
             << " Expected Result: " << resultExpected << " (1 == true)" << endl
             << " Actual Result:   " << resultActual   << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl << endl;
        else                                  cout << " Fail" << endl << endl;

    }   // Test #4 end

    {   // Test #5: Create a string List with 10 items, Insert() at index 11 (out of bounds)
        cout << endl << "Test #5: Create a string List with 10 items, Insert() at 11 (out of bounds)" << endl;
        List<string> testList;

        string strVar;
        for ( int i = 0; i < 10; i++ )
        {
            strVar = "string0" + to_string(i);
            testList.PushBack(strVar);
        }

        cout << " ORIGINAL LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << " " << setw(2)  << (i+1) << ". " << *testList.Get(i) << "\t";
            if ( (i+1) % 4 == 0) cout << endl;
        }
        cout << endl << endl;

        bool resultExpected = false;
        bool resultActual   = (testList.Insert(11, "INSERTED") );

        cout << " MODIFIED LIST: (size " << testList.Size() << ")" << endl;
        for ( int i = 0; i < testList.Size(); i++ )
        {
            cout << " " << setw(2)  << (i+1) << ". " << *testList.Get(i) << "\t";
            if ( (i+1) % 4 == 0) cout << endl;
        }

        cout << endl << endl
             << " Expected Result: " << resultExpected << " (0 == false)" << endl
             << " Actual Result:   " << resultActual   << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl << endl;
        else                                  cout << " Fail" << endl << endl;

    }   // Test #5 end

    {   // Test #6: Create a string List with 100 items, Insert() at index 1 (full array)
        cout << endl << "Test #6: Create a string List with 100 items, Insert() at 1 (full array)" << endl;
        List<string> testList;

        string strVar;
        for ( int i = 0; i < ARRAY_SIZE; i++ )
        {
            strVar = "string" + to_string(i);
            testList.PushBack(strVar);
        }

        cout << " ORIGINAL LIST: (size " << testList.Size() << ")"
             << " | first: " << *testList.Get(0) << " | last: " << *testList.Get(ARRAY_SIZE -1)
             << endl;

        bool resultExpected = false;
        bool resultActual   = (testList.Insert(11, "INSERTED") );

        cout << " MODIFIED LIST: (size " << testList.Size() << ")"
             << " | first: " << *testList.Get(0) << " | last: " << *testList.Get(ARRAY_SIZE -1)
             << endl;

        cout << " Expected Result: " << resultExpected << " (0 == false)" << endl
             << " Actual Result:   " << resultActual   << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl << endl;
        else                                  cout << " Fail" << endl << endl;

    }   // Test #6 end

    {   // Test #7: Create a string List with 10 items, Insert() at index -1 (invalid index)
        cout << endl << "Test #7: Create a string List with 10 items, Insert() at -1 (invalid index)" << endl;
        List<string> testList;

        string strVar;
        for ( int i = 0; i < 10; i++ )
        {
            strVar = "string" + to_string(i);
            testList.PushBack(strVar);
        }

        cout << " ORIGINAL LIST: (size " << testList.Size() << ")"
             << " | first: " << *testList.Get(0) << " | last: " << *testList.Get(9)
             << endl;

        bool resultExpected = false;
        bool resultActual   = (testList.Insert(-1, "INSERTED") );

        cout << " MODIFIED LIST: (size " << testList.Size() << ")"
             << " | first: " << *testList.Get(0) << " | last: " << *testList.Get(9)
             << endl;

        cout << " Expected Result: " << resultExpected << " (0 == false)" << endl
             << " Actual Result:   " << resultActual   << endl;

        if ( resultExpected == resultActual ) cout << " Pass" << endl << endl;
        else                                  cout << " Fail" << endl << endl;

    }   // Test #7 end
}
