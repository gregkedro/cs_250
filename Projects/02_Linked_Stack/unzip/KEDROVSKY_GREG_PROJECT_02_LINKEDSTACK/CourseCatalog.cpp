#include "CourseCatalog.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

#include "UTILITIES/Menu.hpp"
#include "EXCEPTIONS/CourseNotFoundException.hpp"
#include "DATA_STRUCTURES/Stack.hpp" // LinkedStack Class

CourseCatalog::CourseCatalog()
{
    try
    {
        LoadCourses();
    }
    catch ( runtime_error& e )
    {
        cout << endl << "ERROR: Constructor Error." << endl
             << e.what() << endl << endl;
    }
}

void CourseCatalog::LoadCourses() // done
{
    Menu::Header( "LOADING COURSES" );

    ifstream input( "courses.txt" );

    if ( !input.is_open() )
    {
        throw runtime_error( "Error opening input text file, courses.txt" );
    }

    string label, courseCode, courseName, prerequisite;
    Course newCourse; // struct: name, code, prereq (strings) 

    while ( input >> label )
    {
        if ( label == "COURSE" )
        {
            if ( newCourse.name != "" )
            {
                m_courses.PushBack( newCourse );
                newCourse.Clear();
            }
            input >> newCourse.code >> newCourse.name;
        }
        else if ( label == "PREREQ" )
        {
            input >> newCourse.prereq;
        }
    }

    m_courses.PushBack( newCourse ); // added by gk to PushBack the final course
    input.close();

    cout << " * " << m_courses.Size() << " courses loaded" << endl << endl;
}

void CourseCatalog::ViewCourses() noexcept
{
    Menu::Header( "VIEW COURSES" );

    cout << "      Course         "   << endl
         << "  #   |-Prereq    Title" << endl
         << "----  ----------  ---------------------------------------------"
         << endl;

    for ( int i = 0; i < m_courses.Size(); i++ )
    {
        cout << setw(3) << right << (i+1)             << ".  "
             << setw(11) << left  << m_courses[i].code << " "
             << m_courses[i].name << endl;

        if ( m_courses[i].prereq != "" )
            cout << "      |-" << m_courses[i].prereq << endl;

        cout << endl;
    }
}

Course CourseCatalog::FindCourse( const string& code )
{
    for ( int i = 0; i < m_courses.Size(); i++ )
        if ( m_courses[i].code == code ) return m_courses[i];

    throw CourseNotFound( "Unable to find course by code " + code + "." );
}

void CourseCatalog::ViewPrereqs() noexcept
{
    Menu::Header( "GET PREREQS" );

    string userInput;
    cout << " Enter Course Code (then ENTER): ";
    getline(cin, userInput);

    // Check for and fix lower case
    for ( int i = 0; i < (int)userInput.size(); i++ )
    {
        if ( isalpha( userInput[i] ) )
            userInput[i] = toupper( userInput[i] );
    }

    Course current; // declare outside try/catch to maintain scope

    try
    {
        current = FindCourse( userInput );
    }
    catch ( CourseNotFound& e )
    {
        cout << endl << " ERROR: " << e.what() << endl << endl;
        return;
    }
    catch ( exception& e )
    {
        cout << endl << " ERROR: " << e.what() << endl << endl;
        return;
    }

    // If we get past the try/catch, we have a valid course
    LinkedStack<Course> prereqs;

    while ( current.prereq != "" )
    {
        prereqs.Push(current);
        try 
        {
            current = FindCourse(current.prereq);
        }
        catch ( CourseNotFound& e )
        {
            cout << endl << " ERROR: " << e.what() << endl << endl;
            break;
        }
        catch ( exception& e )
        {
            cout << endl << " ERROR: " << e.what() << endl << endl;
            break;
        }
    }
    // Push the last course (with no prereqs) onto the stack:   
    prereqs.Push(current);

    // Display the classes in order of prerequistes
    int counter(1);
    cout << endl << " Classes to take: " << endl;
    while ( prereqs.Size() > 0 )
    {
        cout << setw(3) << right << counter << ".  "
             << setw(8) << left  << prereqs.Top().code 
             << " " << prereqs.Top().name << endl;
        prereqs.Pop();
        counter++;
    }
    cout << endl;
}

void CourseCatalog::Run()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN MENU" );

        int choice = Menu::ShowIntMenuWithPrompt( { "View all courses", "Get course prerequisites", "Exit" } );

        switch( choice )
        {
            case 1:
                ViewCourses();
            break;

            case 2:
                ViewPrereqs();
            break;

            case 3:
                done = true;
            break;
        }
    }
}
