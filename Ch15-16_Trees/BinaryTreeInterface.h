/** Interface for the ADT binary tree
    @file BinaryTreeInterface.h */

#ifndef BINARY_TREE_INTERFACE_H
#define BINARY_TREE_INTERFACE_H

#include "NotFoundException.h"

template<class T>
class BinaryTreeInterface
{
    public: 
    virtual bool isEmpty()                       const =0;
    virtual int  getHeight()                     const =0;
    virtual int  getNumberOfNodes()              const =0;
    virtual T    getRootData()                   const =0;
    virtual void setRootData( const T& newData ) const =0;
    virtual bool add(const T& newData)                 =0;
    virtual bool remove(const T& target)               =0;
    virtual void clear()                               =0;
    virtual T    getEntry(const T& target) const       =0;
    virtual bool contains(const T& target) const       =0;

    /** Traverses the binary tree in preorder, inorder, or postorder,
        and calls the function visit() once for each node.
        @param visit  A client-defined function that performs an 
                      operation on either each visited node or its data
    */
    virtual void preorderTraverse(  void visit(T&) )  const  =0;
    virtual void inorderTraverse(   void visit(T&) )  const  =0;
    virtual void postorderTraverse( void visit(T&) )  const  =0;

    virtual ~BinaryTreeInterface() 
    {
        // intentionally left blank
    }
    
};

#endif
