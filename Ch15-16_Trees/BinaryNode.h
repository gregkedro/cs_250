/** A class of nodes for a link-based binary tree
    @file BinaryNode.h */

#ifndef BINARY_NODE_H
#define BINARY_NODE_H

#include <memory>
using namespace std;

template<class T>
class BinaryNode
{
    private: 
    T                         item;          // data portion
    shared_ptr<BinaryNode<T>> leftChildPtr;  // pointer to left child
    shared_ptr<BinaryNode<T>> rightChildPtr; // pointer to the right child

    public:
    BinaryNode();    
    BinaryNode( const T& anItem );
    BinaryNode( const T& anItem, shared_ptr<BinaryNode<T>> leftPtr,
                                 shared_ptr<BinaryNode<T>> rightPtr);

    void setItem( const T& anItem );
    T    getItem() const;

    bool isLeaf() const;

    auto getLeftChildPtr()  const;
    auto getRightChildPtr() const;

    void setLeftChildPtr(  shared_ptr<BinaryNode<T>> leftPtr  );
    void setRightChildPtr( shared_ptr<BinaryNode<T>> rightPtr );
    
};

// #include "BinaryNode.cpp"

#endif
