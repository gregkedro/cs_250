/** ADT binary tree: Linked-based implementation
    @file BinaryNodeTree.cpp */

#include "BinaryTreeInterface.h"
#include "BinaryNode.h"
#include "PrecondViolatedExcept.h"
#include "NotFoundException.h"
#include "BinaryNodeTree.h"
#include <memory>
using namespace std;

//-----------------------------------------------------------------
//      Constructor and Destructor Section
//-----------------------------------------------------------------

template<class T>
BinaryNodeTree<T>::BinaryNodeTree() : rootPtr(nullptr) { }

template<class T>
BinaryNodeTree<T>::BinaryNodeTree( const T& rootItem ) : 
    rootPtr( make_shared<BinaryNode<T>>(rootItem, nullptr, nullptr ) { }

template<class T>
BinaryNodeTree<T>::BinaryNodeTree( const T& rootItem, 
                                   const shared_ptr<BinaryNode<T>> leftTreePtr,
                                   const shared_ptr<BinaryNode<T>> rightTreePtr) :
    rootPtr( make_shared<BinaryNode<T>>(rootItem, 
                                        copyTree(leftTreePtr->rootPtr),
                                        copyTree(rightTreePtr->rootPtr) )  {  }

template<class T>
BinaryNodeTree<T>::BinaryNodeTree( const shared_ptr<BinaryNode<T>>& tree> )
{
    rootPtr = copyTree( tree, rootPtr );
}


// Copies the tree rooted at treePtr and returns a pointer to the root of the copy
template<class T>
share_ptr<BinaryNode<T>> BinaryNodeTree<T>::copyTree(const shared_ptr<BinaryNode<T>> oldTreeRootPtr const
{
    shared_ptr<BinaryNode<T>> newTreePtr;

    // Copy tree nodes during a preorder traverse
    if ( oldTreeRootPtr != nullptr )
    {
        // Copy node
        newTreePtr = make_shared<BinaryNode<T>>( oldTreeRootPtr->getItem(), nullptr, nullptr );
        newTreePtr->setLeftChildPtr( copyTree( oldTreeRootPtr->getLeftChildPtr()  ) );
        newTreePtr->setRightChildPtr(copyTree( oldTreeRootPtr->getRightChildPtr() ) );
    }

    return newTreePtr;
}

// Destructor
template<class T>
BinaryNodeTree<T>:: ~BinaryNodeTree()
{
    destroyTree( rootPtr ); 
}

// Recursively deletes all nodes from the tree
template<class T>
void BinaryTreeNode<T>::destroyTree( shared_ptr<BinaryNode<T>> subTreePtr )
{
    if ( subTreePtr != nullptr )
    {
        destroyTree( subTreePtr->getLeftChildPtr()  );
        destroyTree( subTreePtr->getRightChildPtr() );
        subTreePtr.reset() // decrement smart pointer reference count to node
    }
}

//-----------------------------------------------------------------
//      Protected Utility Methods Section: 
//      Recursive helper methods for the public methods
//-----------------------------------------------------------------

template<class T>
int BinaryTreeNode<T>::getHeightHelper( shared_ptr<BinaryNode<T>> subTreePtr ) const
{

}







int getNumberOfNodesHelper( shared_ptr<BinaryNode<T> subTreePtr  ) const;
    
    // Recursively adds a new node to the tree in a left/right fashion to keep tree balanced
    auto balancedAdd( share_ptr<BinaryNode<T>> subTreePtr, shared_ptr<BinaryNode<T>> newNodePtr );

    // Removes the target value from the tree
    virtual auto removeValue( shared_ptr<BinaryNode<T>> subTreePtr, const T target, bool& isSuccessful );

    // Copies values up the tree to overwrite value in current node until a leaf is
    //  reached; the leaf is then removed, since its value is stored in the parent.
    auto moveValueUpTree( shared_ptr<BinaryNode<T>> subTreePtr );

    // Recursively searches for target value
    virtual auto findNode( shared_ptr<BinaryNode<T>> treePtr, const T& target, bool& isSuccessful ) const;

    // Recursive traversal helper methods
    void preorder(  void visit(T&), shared_ptr<BinaryNode<T>> treePtr ) const;
    void inorder(   void visit(T&), shared_ptr<BinaryNode<T>> treePtr ) const;
    void postorder( void visit(T&), shared_ptr<BinaryMode<T>> treePtr ) const;

    //-----------------------------------------------------------------
    //      Public BinaryTreeInterface Methods Section
    //-----------------------------------------------------------------
    bool isEmpty() const;
    int  getHeight() const;
    int  getNumberOfNodes() const;
    T    getRootData() const;
    void setRootData( const T& newData );
    bool add( const T& newData );            // adds an item to the tree
    bool remove( const T& data );            // removes specified item from the tree
    void clear();
    T    getEntry( const T& anEntry );
    bool contains( const T& anEntry ) const;
    
    //-----------------------------------------------------------------
    //      Public Traversals Section
    //-----------------------------------------------------------------
    void preorderTraverse( void visit(T&) ) const;
    void inorderTraverse(  void visit(T&) ) const;
    void postorderTravers( void visit(T&) ) const;

    //-----------------------------------------------------------------
    //      Overloaded Operator Section
    //-----------------------------------------------------------------
    BinaryNodeTree& operator=( const BinaryNodeTree& rightHandSide ); // overloaded assignment operator

};

// #include "BinaryNodeTree.cpp"

#endif
