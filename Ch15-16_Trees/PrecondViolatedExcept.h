/** Programmer specific exception
    @file NotFoundException.h */

#ifndef PRECOND_VIOLATED_EXCEPT_H
#define PRECOND_VIOLATED_EXCEPT_H

#include <stdexcept>
#include <string>
using namespace std;

class PrecondViolatedExcept : public exception
{
    public:
    // constructor
    PrecondViolatedExcept( const string& message = "") : exception("Predcondition Violated! " + message )
    {
        // intentionally left blank
    }
};

#endif
