#ifndef TREE_NODE_H
#define TREE_NODE_H

template<class T>
class TreeNode
{
    private:
    T   item;       // data portion
    int leftChild;  // index to left child
    int rightChild; // indext to right child

    public:
    TreeNode();
    TreeNode( const T& nodeItem, int left, int right );

    // implementation of all other methods not included

};

#endif

