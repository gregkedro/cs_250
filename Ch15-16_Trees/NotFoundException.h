/** Programmer specific exception
    @file NotFoundException.h */

#ifndef NOT_FOUND_EXCEPTION_H
#define NOT_FOUND_EXCEPTION_H

#include <stdexcept>
#include <string>
using namespace std;

class NotFoundException : public exception
{
    public:

    // constructor
    NotFoundException( const string& message = "") : exception("Target not found: " + message )
    {
        // intentionally left blank
    }

    /*
    USAGE: 

    1. In the function that might have a problem, in an if statement:
        throw NotFoundException( "whatever string message you want displayed" );

    2. Then, from whence that function was called:
        try
        {
            funkyFunction();
        }
        catch( NotFoundException e )
        {
            cout << what.e() << endl;
        }

    */

};

#endif
