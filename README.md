#GKedro :: CS-250 Repo
---
## First to-do on JCCC Computer: 
Set your user name and email address (every Git commit uses this information and it is immutably baked into the commits you start creating):
```
$ git config --global user.name "GKedro"  
$ git config --global user.email gregkedro@gmail.com
```
[Set-up](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup). This will reset everytime the JCCC computer is rebooted, so you have to configure it each time you sit at a machine.

##Links
-[Rachel's GitHub](https://github.com/Rachels-Courses/CS250-Data-Structures)  
-[Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) | [Common Markdown](http://commonmark.org/help/)  
-[Git Book: Pro Git](https://git-scm.com/book/en/v2)  
-[Basic Git Commands](https://www.atlassian.com/git/tutorials/svn-to-git-prepping-your-team-migration#basic-git-commands)  
-[C++ Cheat Sheets](https://github.com/GregKedrovsky/c-plus-plus)  