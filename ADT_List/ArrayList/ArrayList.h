/** ADT List: Array-Based Implementation
    @file ArrayList.h */

#ifndef ARRAY_LIST_
#define ARRAY_LIST_

#include <string>
#include "../Shared/ListInterface.h"
#include "../Shared/PrecondViolatedExcept.h"

template<typename T>
class ArrayList : public ListInterface<T>
{
    private:
    static const int DEFAULT_CAPACITY = 100;    // Default capacity of the List
    T items[DEFAULT_CAPACITY +1];               // Array of list items (ignore items[0])
    int itemCount;                              // Current count of list items
    int maxItems;                               // Maximum capacity of the List

    public:
    ArrayList();
    // Copy constructor and Destructor are supplied by the compiler

    bool isEmpty() const;
    int getLength() const;
    bool insert( int newPosition, const T& newEntry );
    bool remove( int position );
    void clear();

    T getEntry( int position ) const;
    T replace(int position, const T& newEntry);

    void display();

};

#include "ArrayList.cpp"

#endif
