#include <iostream>
#include <string>
#include "ArrayList.h"

// Implementation

template<typename T>
ArrayList<T>::ArrayList() : itemCount(0), maxItems(DEFAULT_CAPACITY)
{
    // intentionally left blank
}
// Copy constructor and Destructor are supplied by the compiler

template<typename T>
bool ArrayList<T>::isEmpty() const
{
    return itemCount == 0;
}

template<typename T>
int ArrayList<T>::getLength() const
{
    return itemCount;
}

template<typename T>
bool ArrayList<T>::insert( int newPosition, const T& newEntry )
{
    bool ableToInsert = ( newPosition >= 1)              && // valid index
                        ( newPosition <= itemCount + 1 ) && // within range of existing items
                        ( itemCount < maxItems);            // there is room in the list

    if ( ableToInsert )
    {
        for ( int pos = itemCount; pos >= newPosition; pos-- )
        {
            items[pos+1] = items[pos];
        }

        items[newPosition] = newEntry;
        itemCount++;
    }

    return ableToInsert;
}

template<typename T>
bool ArrayList<T>::remove( int position )
{
    return false;
}

template<typename T>
void ArrayList<T>::clear()
{
    itemCount = 0;
}

template<typename T>
T ArrayList<T>::getEntry( int position ) const
{
    if ( position < 1 || position > itemCount )
    {
        std::string message = ": getEntr() called with an empty list or invalid position";
        throw PrecondViolatedExcept( message );
    }
    else
    {
        return items[position];
    }
}

template<typename T>
T ArrayList<T>::replace(int position, const T& newEntry)
{
    return items[0];
}

template<typename T>
void ArrayList<T>::display()
{
    for ( int i = 1; i <= itemCount; i++ )
    {
        std::cout << " " << i << ". " << items[i] << std::endl;
    }
    return;
}
