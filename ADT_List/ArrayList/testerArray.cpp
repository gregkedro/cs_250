#include <iostream>
#include <stdexcept>
#include <string>

#include "ArrayList.h"

using namespace std;

int main()
{
    ArrayList<string> myArray;

    cout << endl
         << "-----------------------------------------------------------------------" << endl
         << "|                          TESTER: ArrayList                          |" << endl
         << "-----------------------------------------------------------------------" << endl << endl;

    cout << "TEST: isEmpty()"   << endl
         << " Should: 1 (true)" << endl
         << " Actual: " << myArray.isEmpty() << endl << endl;

    cout << "TEST: itemCount()" << endl
         << " Should: 0"        << endl
         << " Actual: " << myArray.getLength() << endl << endl;

    cout << "TEST: insert()"    << endl
         << " Insert() five items..." << endl;
    
         myArray.insert( 1, "one" );
         myArray.insert( 2, "two" );
         myArray.insert( 3, "three" );
         myArray.insert( 4, "four" );
         myArray.insert( 5, "five" );

    cout << " getLength" << endl
         << "  Should: 5"  << endl
         << "  Actual: "   << myArray.getLength() << endl
         << " getEntry(1..5):" << endl;

         myArray.display();
         cout << endl;

    cout << " Exception: getEntry() with invalid argument" << endl;
         try
         {
            myArray.getEntry(6);
         }
         catch ( PrecondViolatedExcept& e )
         { 
            cout << e.what() << endl << endl;
         }

    cout << "TEST: insert() between existing items" << endl
         << " Insert HERE at position 3:" << endl;

         myArray.insert( 3, "HERE" );
         myArray.display();

    cout << endl;

    return 0;
}
