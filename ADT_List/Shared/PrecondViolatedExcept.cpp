#include "PrecondViolatedExcept.h"

// constructor
PrecondViolatedExcept::PrecondViolatedExcept( const std::string& message )
    : std::logic_error(" Precondition Violated Exception " + message)
{
    // intentionally left blank
}
